﻿namespace Elder.Data.Tests
{
    using NUnit.Framework;

    [TestFixture]
    public class Tests
    {
        [Test]
        public void Test1()
        {
        }
    }

    public interface IDataService<T> where T : IEntity
    {
        void Save(T record);

        void Get(int id);
    }

    public interface IStorage<T> where T : IEntity
    {
        void Save(T record);

        void Get(int id);
    }

    public interface IFileStorage<T> : IStorage<T> where T : IEntity
    {
    }

    public interface IEntity
    {
        int Id { get; }
    }
}