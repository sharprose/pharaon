namespace Elder.Game.Services
{
    using System.Collections.Generic;

    public interface IGamePipeline
    {
        int Order { get; }

        string Name { get; }

        string Description { get; }

        void Run(IEnumerable<IGamePipeline> previos);
    }
}