namespace Elder.Game.Pipelines
{
    using System.Collections.Generic;

    using Gear.DI.Services;

    using Services;

    public class InitializeCorePipeline : IGamePipeline
    {
        public InitializeCorePipeline(IContainer container)
        {
            this._container = container;
        }

        private IContainer _container { get; }

        public int Order => 0;

        public string Name => nameof(InitializeCorePipeline);

        public string Description => "";

        public void Run(IEnumerable<IGamePipeline> previos)
        {
        }
    }
}