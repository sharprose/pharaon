namespace Elder.Game
{
    using Gear.DI;
    using Gear.DI.Services;
    using Gear.Moduling;

    using Pipelines;

    using Services;

    public class Module : IModule
    {
        public string Code => "Elder.Game";

        public void Register(IContainer container)
        {
            container.AddToCollection<IGamePipeline>(
                new[] {typeof(InitializeCorePipeline)},
                ComponentLifestyleType.Singleton);
        }
    }
}