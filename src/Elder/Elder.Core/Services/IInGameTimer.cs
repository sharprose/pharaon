namespace Elder.Core.Services
{
    public interface IInGameTimer
    {
        void Start();

        void Stop();
    }
}