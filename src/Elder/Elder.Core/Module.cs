namespace Elder.Core
{
    using Gear.DI.Services;
    using Gear.Moduling;

    public class Module : IModule
    {
        public string Code => "Elder.Core";

        public void Register(IContainer container)
        {
            
        }
    }
}