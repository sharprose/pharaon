namespace Elder.Controller.Services
{
    using UnityEngine.SceneManagement;

    public class SceneController : ISceneController
    {
        private readonly string path = "Scenes/";

        public void Load(string name)
        {
            SceneManager.LoadScene($"{this.path}/{name}");
        }
    }
}