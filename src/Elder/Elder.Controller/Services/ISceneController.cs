namespace Elder.Controller.Services
{
    public interface ISceneController : IController
    {
        void Load(string name);
    }
}