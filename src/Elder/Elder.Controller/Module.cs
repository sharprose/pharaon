namespace Elder.Controller
{
    using Gear.DI.Services;
    using Gear.Moduling;

    using Services;

    public class Module : IModule
    {
        public string Code => "Elder.Controller";

        public void Register(IContainer container)
        {
            container.Register<ISceneController, SceneController>();
        }
    }
}