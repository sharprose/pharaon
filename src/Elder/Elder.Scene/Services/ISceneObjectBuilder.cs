namespace Elder.Scene.Services
{
    using Entities;
    using Models;

    public interface ISceneObjectBuilder
    {
        InGameObject Build(SceneObject sceneObject);
    }
}