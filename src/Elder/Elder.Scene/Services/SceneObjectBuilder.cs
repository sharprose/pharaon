namespace Elder.Scene.Services
{
    using System.Collections.Generic;
    using Entities;
    using Gear.DI.Services;
    using Models;
    using UnityEngine;

    public abstract class SceneObjectBuilder : ISceneObjectBuilder
    {
        
        
        public SceneObjectBuilder(IContainer container)
        {
            
        }
        private Dictionary<string, Object> _resources = new Dictionary<string, Object>(50);
        
        
        
        public InGameObject Build(SceneObject sceneObject)
        {
            var path = $"Prefabs/{sceneObject.Type}/{sceneObject}/template.prefab";

            var resource = Release(path);
            
            var result = GameObject.Instantiate(resource) as InGameObject;
            
            return result;
        }
        
        private UnityEngine.Object Release(string path)
        {
            if (_resources.ContainsKey(path))
            {
                return _resources[path];
            }

            var result = Resources.Load(path);
            
            _resources.Add(path, result);
            
            return result;
        }
    }
}