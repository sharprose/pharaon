namespace Elder.Scene.Services
{
    public interface ITypeBinder
    {
        string Type { get; }
    }
}