namespace Elder.Scene.Managers
{
    using System;
    using System.Collections.Generic;
    using Entities;
    using Gear.DI.Services;
    using Models;
    using Services;
    using UnityEngine.SceneManagement;
    using uSceneManager = UnityEngine.SceneManagement.SceneManager;
    
    public class SceneManager : ISceneManager
    {
        public SceneManager(IContainer container)
        {
            _data = new Dictionary<long, InGameObject>(10);
            _sceneObjectBuilder = container.GetInstance<ISceneObjectBuilder>();
        }

        private readonly Dictionary<long, InGameObject> _data;
        
        private ISceneObjectBuilder _sceneObjectBuilder { get; }
        
        public InGameObject Get(long id)
        {
            if (_data.ContainsKey(id))
            {
                return _data[id];
            }

            return null;
        }

        public InGameObject Get(SceneObject sceneObject)
        {
            return Get(sceneObject.Id);
        }

        public InGameObject Append(SceneObject sceneObject)
        {
            var result = _sceneObjectBuilder.Build(sceneObject);
            
            return result;
        }

        public void Load(string name)
        {
            uSceneManager.LoadScene(name, new LoadSceneParameters()
            {
                loadSceneMode = LoadSceneMode.Single,
                localPhysicsMode = LocalPhysicsMode.None
            });
        }
    }
}