namespace Elder.Scene.Managers
{
    using Entities;
    using Models;

    public interface ISceneManager
    {
        InGameObject Get(long id);
        
        InGameObject Get(SceneObject sceneObject);

        InGameObject Append(SceneObject sceneObject);

        void Load(string name);
    }
}