namespace Elder.Scene
{
    using Gear.Domain.Services;

    public interface IGameMigration
    {
        string Code { get; }
        
        void Up(DataProviderFactory dataProviderFactory);
    }
}