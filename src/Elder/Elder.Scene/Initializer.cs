namespace Elder.Scene
{
    using Entities;
    using Gear.DI.Services;
    using Gear.Domain;
    using Gear.Moduling.Services;

    public class Initializer : IInitializer
    {
        public int Order => 1;
        
        public void Initialize(IContainer container)
        {
            container.RegisterEntity<ObjectData>();
            container.RegisterEntity<SceneDescription>();
            container.RegisterEntity<SceneObject>();
        }
    }
}