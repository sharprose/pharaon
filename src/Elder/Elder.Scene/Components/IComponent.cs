namespace Elder.Scene.Components
{
    using Models;

    public interface IComponent<IData>
        where IData : class, IComponentData
    {
        string Name { get; }

        void Set(InGameObject inGameObject);
        
        void Get(InGameObject inGameObject);
    }
}