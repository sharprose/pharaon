namespace Elder.Scene.Models
{
    using System;
    using System.Collections.Generic;
    
    using Entities;
    using Gear.Storage.Models;
    
    using UnityEngine;

    public class InGameObject : MonoBehaviour
    {
        private Dictionary<Type, Component> _componentCashe = new Dictionary<Type, Component>(10);
        
        private ObjectData Data { get; }

        public Dictionary<string, DynamicRow> this[string key]
        {
            get
            {
                if (!this.Data.Data.ContainsKey(key))
                {
                    return null;
                }
                
                return Data.Data[key];
            }
        }
        
        public T GetCashedComponent<T>()
            where T : Component
        {
            var componentType = typeof(T);

            if (_componentCashe.ContainsKey(componentType))
            {
                return _componentCashe[componentType] as T;
            }

            var component = GetComponent<T>();

            if (component as object != null)
            {
                _componentCashe.Add(componentType, component);
            }

            return component;
        }

        public T PushComponent<T>()
            where T : Component
        {
            return gameObject.AddComponent<T>();
        }
    }
}