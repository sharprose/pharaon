namespace Elder.Scene.Entities
{
    using System.Collections.Generic;

    using Gear.Storage;
    using Gear.Storage.Entities;
    using Gear.Storage.Models;

    [EntityDescription(StorageType = StorageType.File)]
    public class ObjectData : BaseEntity
    {
        public ObjectData()
        {
            this.Data = new Dictionary<string, Dictionary<string, DynamicRow>>(15);    
        }
        
        public int SceneObjectId { get; set; }
        
        public Dictionary<string, Dictionary<string, DynamicRow>> Data { get; set; }
    }
}