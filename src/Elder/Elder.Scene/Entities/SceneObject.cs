namespace Elder.Scene.Entities
{
    using Gear.Storage;
    using Gear.Storage.Entities;

    [EntityDescription(StorageType = StorageType.File)]
    public class SceneObject : BaseEntity
    {
        public string Type { get; set; }
        
        public string Name { get; set; }
    }
}