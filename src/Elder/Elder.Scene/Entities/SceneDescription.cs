namespace Elder.Scene.Entities
{
    using Gear.Storage;
    using Gear.Storage.Entities;

    [EntityDescription(StorageType = StorageType.File)]
    public class SceneDescription : BaseEntity
    {
        public string Name { get; set; }
        
        public string Path { get; set; }
    }
}