namespace Elder.Scene
{
    using Gear.DI.Services;
    using Gear.Moduling;
    using Managers;

    public class Module : IModule
    {
        public string Code => "Elder.Scene";
        
        public void Register(IContainer container)
        {
            container.Register<ISceneManager, SceneManager>();
        }
    }
}