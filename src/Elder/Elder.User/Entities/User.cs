namespace Elder.User.Entities
{
    using Gear.Data;

    public class User : IEntity
    {
        public string Login { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public int Id { get; set; }

        public override string ToString()
        {
            return this.Login;
        }
    }
}