namespace Elder.User.Services
{
    using Entities;

    using Gear.Data.Services;
    using Gear.DI.Services;

    public class UserProvider : DataProvider<User>, IUserProvider
    {
        public UserProvider(IContainer container) : base(container)
        {
        }
    }
}