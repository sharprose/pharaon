namespace Elder.User.Services
{
    using Entities;

    using Gear.Data.Services;

    public interface IUserProvider : IDataProvider<User>
    {
    }
}