namespace Elder.User.Models
{
    using System;

    using Entities;

    public class UserProfile
    {
        public User User { get; set; }

        public DateTimeOffset LastLogin { get; set; }

        public override string ToString()
        {
            return this.User.Login;
        }
    }
}