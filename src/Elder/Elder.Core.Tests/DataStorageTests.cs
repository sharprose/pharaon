﻿namespace Elder.Core.Tests
{
    using NUnit.Framework;

    using Storage;

    [TestFixture]
    public class DataStorageTests
    {
        [Test]
        public void Test()
        {
            var storage = new DynamicDataStorage();

            storage["int"].Set("a", 123);
            storage["int"].Set("b", 321);

            var valueIntA = storage["int"].Get<int>("a");
            var valueIntB = storage["int"].Get<int>("B");

            Assert.AreEqual(valueIntA, 123);
            Assert.AreEqual(valueIntB, 321);
        }
    }
}