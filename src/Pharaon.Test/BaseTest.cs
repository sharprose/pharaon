namespace Pharaon.Test
{
    using System;
    using System.Diagnostics;

    using Gear.DI.Services;
    using Gear.DI.SimpleInj.Services;
    using Gear.Moduling.Services;

    public abstract class BaseTest
    {
        private Stopwatch _stopwatch;

        public BaseTest()
        {
            this.InitializeContainer();
        }

        protected IContainer Container { get; private set; }

        ~BaseTest()
        {
            this.Container.Dispose();
        }

        private void InitializeContainer()
        {
            this.Container = new SimpleContainer();
            var activator = new ModuleActivator(this.Container);

            activator.ActivateModules();
        }

        protected void StopwatchStart()
        {
            this._stopwatch = new Stopwatch();
            this._stopwatch.Start();
        }

        protected void StopwatchStop()
        {
            if (this._stopwatch == null)
            {
                return;
            }

            this._stopwatch.Stop();

            var ts = this._stopwatch.Elapsed;

            Console.WriteLine($"Stopwatch: {ts.Hours.ToString()}:{ts.Minutes.ToString()}:{ts.Seconds} {ts.Milliseconds} ");

            this._stopwatch = null;
        }
    }
}