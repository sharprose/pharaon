﻿namespace Gear.Moduling.Tests
{
    using DI.Services;
    using DI.SimpleInj.Services;

    using NUnit.Framework;

    using Serialization.Services;

    using Services;

    [TestFixture]
    public class Tests
    {
        [Test]
        public void Test1()
        {
            IContainer container = new SimpleContainer();
            var activator = new ModuleActivator(container);

            activator.ActivateModules();

            var serializer = container.GetInstance<IJsonSerializer>();

            Assert.NotNull(serializer);
        }
    }
}