namespace Gear.Configuration.Services
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Configurations;
    using DI.Services;
    using Files.Json;

    public class ConfigurationService : IConfigurationService
    {
        public ConfigurationService(IContainer container)
        {
            _container = container;
            _fileresolver = container.GetInstance<IFileResolver>();
            
            Load();
        }

        private IContainer _container;

        private string _path { get; }
        
        private IFileResolver _fileresolver;
        //private string _fileName = "config.cfg";
        
        private Dictionary<Type, object> _items = new Dictionary<Type, object>(5);
        
        public T Get<T>() where T : class, new()
        {
            T result;
            var type = typeof(T);

            if (_items.ContainsKey(type))
            {
                result = _items[type] as T;
            }
            else
            {
                result = new T();
                _items.Add(type, result);
            }

            return result;
        }

        public void Set<T>(T configurationItem) where T : class, new()
        {
            var type = typeof(T);

            if (!_items.ContainsKey(type))
            {
                _items.Add(type, null);
            }

            _items[type] = configurationItem;
        }

        public void Save()
        {
            var ac = Get<ApplicationConfig>();
            _fileresolver.SaveToText(_items, ac.RootPath, "cfg");
        }

        ~ConfigurationService()
        {
            this.Save();
            _container = null;
        }

        private void Load()
        {
            var ac = Get<ApplicationConfig>();
            var path = $"{ac.RootPath}\\cfg.json";
            
            Dictionary<Type, object> cfg = new Dictionary<Type, object>();
            
            if (File.Exists(path))
            {
                cfg = _fileresolver.LoadFromText<Dictionary<Type, Object>>(ac.RootPath, "cfg");
            }
            
            _items = cfg;
        }
    }
}