namespace Gear.Configuration.Services
{
    public interface IConfigurationService
    {
        T Get<T>() where T : class, new();
        
        void Set<T>(T configurationItem) where T : class, new();

        void Save();
    }
}