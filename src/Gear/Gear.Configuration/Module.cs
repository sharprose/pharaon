namespace Gear.Configuration
{
    using DI;
    using DI.Services;
    using Moduling;
    using Services;

    public class Module : IModule
    {
        public string Code => "Gear.Configuration";
        
        public void Register(IContainer container)
        {
            container.Register<IConfigurationService, ConfigurationService>(ComponentLifestyleType.Singleton);
        }
    }
}