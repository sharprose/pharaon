namespace Gear.Configuration.Configurations
{
    using System.IO;

    public class ApplicationConfig
    {
        public string RootPath { get; set; }
        
        public bool IsDebug { get; set; }

        public ApplicationConfig()
        {
            var type = typeof(ApplicationConfig);
            var path = new FileInfo(type.Assembly.FullName).DirectoryName;

            RootPath = path;
            IsDebug = true;
        }
    }
}