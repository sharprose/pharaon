namespace Gear.Serialization.Services
{
    /// <summary>
    ///     Json сериализатор
    /// </summary>
    public interface IJsonSerializer
    {
        /// <summary>
        ///     Объект в Json строку
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        string Serialize(object data);

        /// <summary>
        ///     Json строку в объект
        /// </summary>
        /// <param name="data"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T Deserialize<T>(string data);

        object Deserialize(string data);
    }
}