namespace Gear.Files.Tests.Models
{
    public class FileModel
    {
        public int Id { get; set; }

        public long Value { get; set; }

        public string Name { get; set; }

        public char[] Chars { get; set; }

        public FileModel Clone()
        {
            var result = new FileModel
            {
                Id = this.Id,
                Name = this.Name,
                Chars = this.Chars,
                Value = this.Value
            };

            return result;
        }
    }
}