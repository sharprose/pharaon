namespace Gear.Files.Tests
{
    using System;
    using System.Text;

    using Models;

    public static class ModelBuilder
    {
        private static readonly char[] _chars = new char[25]
        {
            'a',
            'b',
            'c',
            'd',
            'e',
            'f',
            'g',
            'h',
            'j',
            'k',
            'l',
            'z',
            'x',
            'v',
            'n',
            'm',
            'q',
            'w',
            'r',
            't',
            'y',
            'u',
            'i',
            'o',
            'p'
        };

        public static FileModel BuildModel()
        {
            var random = new Random(DateTime.Now.Millisecond);
            var chars = new char[random.Next(0, 15)];
            var str = new StringBuilder();

            for (var i = 0; i < chars.Length; i++)
            {
                chars[i] = _chars[random.Next(0, _chars.Length)];
            }

            for (var i = 0; i < 150; i++)
            {
                str.Append(_chars[random.Next(0, _chars.Length)]);
            }

            var result = new FileModel
            {
                Id = random.Next(0, int.MaxValue),
                Chars = chars,
                Name = str.ToString()

                //Value = 
            };

            return result;
        }
    }
}