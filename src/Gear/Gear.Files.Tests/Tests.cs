﻿namespace Gear.Files.Tests
{
    using System.IO;

    using Newtonsoft.Json;

    using NUnit.Framework;

    using Pharaon.Test;

    using TextWriter = Json.TextWriter;

    [TestFixture]
    public class Tests : BaseTest
    {
        [Test]
        public void Write()
        {
            this.StopwatchStart();

            var writer = new TextWriter();

            for (var i = 0; i < 100; i++)
            {
                var model = ModelBuilder.BuildModel();
                var name = $"{string.Concat(model.Chars)}.json";
                var data = JsonConvert.SerializeObject(model);

                writer.Write(Directory.GetCurrentDirectory(), name, data);
            }

            this.StopwatchStop();
        }

        [Test]
        public void WriteAsync()
        {
            this.StopwatchStart();

            var writer = new TextWriter();

            for (var i = 0; i < 100; i++)
            {
                var model = ModelBuilder.BuildModel();
                var name = $"{string.Concat(model.Chars)}.json";
                var data = JsonConvert.SerializeObject(model);

                writer.WriteAsync(Directory.GetCurrentDirectory(), name, data);
            }

            this.StopwatchStop();
        }
    }
}