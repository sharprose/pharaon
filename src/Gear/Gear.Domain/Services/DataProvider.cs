namespace Gear.Domain.Services
{
    using System.Collections.Generic;
    using DI.Services;
    using Storage.Entities;
    using Storage.Services;

    public class DataProvider<T> : IDataProvider<T>
        where T : class, IEntity
    {
        public DataProvider(IContainer container)
        {
            var storageFactory = container.GetInstance<IStorageFactory>();

            _storage = storageFactory.Get<T>();
        }

        private ICrudStorage _storage { get; set; }

        public T Get(long id)
        {
            return _storage.Get<T>(id);
        }

        public IEnumerable<T> Get(params long[] ids)
        {
            return _storage.Get<T>(ids);
        }

        public IEnumerable<T> GetAll()
        {
            return _storage.GetAll<T>();
        }

        public void Save(T entity)
        {
            _storage.Save(entity);
        }

        public void Save(IEnumerable<T> entities)
        {
            _storage.Save(entities);
        }

        public void Delete(params long[] ids)
        {
            _storage.Delete(ids);
        }

        ~DataProvider()
        {
            _storage = null;
        }
    }
}