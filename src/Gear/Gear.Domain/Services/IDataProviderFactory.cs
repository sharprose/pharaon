namespace Gear.Domain.Services
{
    using Storage.Entities;

    public interface IDataProviderFactory
    {
        IDataProvider<T> Get<T>() where T : class, IEntity;
    }
}