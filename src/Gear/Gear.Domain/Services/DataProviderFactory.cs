namespace Gear.Domain.Services
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    using DI.Services;

    using Storage.Entities;

    public class DataProviderFactory : IDataProviderFactory
    {
        private Dictionary<Type, object> _providers = new Dictionary<Type, object>();
        
        public DataProviderFactory(IContainer container)
        {
            this._container = container;
        }

        private IContainer _container { get; }
        
        public IDataProvider<T> Get<T>() where T : class, IEntity
        {
            var type = typeof(T);

            if (this._providers.ContainsKey(type))
            {
                return this._providers[type] as IDataProvider<T>;
            }
            
            var provider = new DataProvider<T>(_container);

            this._providers.Add(type, provider);
            
            return provider;
        }
    }
}