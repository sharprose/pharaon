namespace Gear.Domain.Services
{
    using System.Collections.Generic;
    using Storage.Entities;

    public interface IDataProvider<T>
        where T : class, IEntity
    {
        T Get(long id);
        
        IEnumerable<T> Get(params long[] ids);
        
        IEnumerable<T> GetAll();

        void Save(T entity);
        
        void Save(IEnumerable<T> entities);
        
        void Delete(params long[] ids);
    }
}