namespace Gear.Domain
{
    using DI;
    using DI.Services;

    using Moduling;

    using Services;

    public class Module : IModule
    {
        public string Code { get; }

        public void Register(IContainer container)
        {
            container.Register<IDataProviderFactory, DataProviderFactory>(ComponentLifestyleType.Singleton);
        }
    }
}