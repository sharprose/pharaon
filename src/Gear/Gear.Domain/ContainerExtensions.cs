namespace Gear.Domain
{
    using System.Data;

    using DI.Services;

    using Services;

    using Storage;
    using Storage.Entities;

    public static class ContainerExtensions
    {
        public static void RegisterEntity<T>(this IContainer container)
            where T : class, IEntity
        {
            container.RegisterStorage<T>();
        }
    }
}