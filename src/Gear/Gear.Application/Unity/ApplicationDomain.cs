﻿namespace Gear.Application.Unity
{
    using System;
    using System.Management.Instrumentation;
    using DI.Services;
    using DI.SimpleInj.Services;

    using Moduling.Services;

    public class ApplicationDomain
    {
        private static ApplicationDomain _instance;

        public IContainer Container => _container;

        private static IContainer _container;
        
        private static object lockObject = new Object();
        
        public static ApplicationDomain Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        if (_instance == null)
                        {
                            _instance = new ApplicationDomain();   
                        }   
                    }
                }

                return _instance;
            }
        }

        public ApplicationDomain()
        {
            _container = new SimpleContainer();
            
            var activator = new ModuleActivator(_container, string.Empty);
            activator.ActivateModules();
        }
    }
}