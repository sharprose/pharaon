namespace Gear.Application
{
    using System.IO;
    using Configuration.Configurations;
    using Configuration.Services;
    using DI.Services;
    using Moduling.Services;
    using Unity;

    public class Initializer : IInitializer
    {
        public int Order => 0;
        
        public void Initialize(IContainer container)
        {
            var configurationManager = container.GetInstance<IConfigurationService>();
            var appCfg = configurationManager.Get<ApplicationConfig>();
                
            var asm = typeof(Initializer).Assembly.Location;
            var fileInfo = new FileInfo(asm);
            var path = $"{fileInfo.Directory.FullName}\\";
            
            appCfg.RootPath = path;
        }
    }
}