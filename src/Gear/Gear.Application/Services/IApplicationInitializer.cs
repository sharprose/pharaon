namespace Gear.Application.Services
{
    using DI.Services;

    public interface IApplicationInitializer
    {
        void Run(IContainer container);
    }
}