namespace Gear.Files
{
    using System;
    using System.IO;
    using System.Threading.Tasks;

    public abstract class BaseReader
    {
        protected abstract Task<string> InnerRead(string directory, string filename);

        public string Read(string directory, string filename)
        {
            this.BeforeRead(directory, filename);

            var task = this.InnerRead(directory, filename);

            task.Wait();

            return task.Result;
        }

        public async Task<string> ReadAsync(string directory, string filename)
        {
            this.BeforeRead(directory, filename);

            return await this.InnerRead(directory, filename);
        }

        private void BeforeRead(string directory, string filename)
        {
            if (string.IsNullOrEmpty(directory) ||
                string.IsNullOrEmpty(filename))
            {
                throw new ArgumentException();
            }

            if (!Directory.Exists(directory))
            {
                throw new DirectoryNotFoundException(directory);
            }

            var path = $"{directory}\\{filename}";

            if (!File.Exists(path))
            {
                throw new FileNotFoundException(path);
            }
        }
    }
}