namespace Gear.Files
{
    using System.Threading.Tasks;

    public interface IFileReader
    {
        string Read(string directory, string filename);

        Task<string> ReadAsync(string directory, string filename);
    }
}