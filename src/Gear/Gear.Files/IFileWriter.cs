namespace Gear.Files
{
    using System.Threading.Tasks;

    public interface IFileWriter
    {
        void Write(string directory, string filename, string data, bool rewrite = true);

        Task WriteAsync(string directory, string filename, string data, bool rewrite = true);
    }
}