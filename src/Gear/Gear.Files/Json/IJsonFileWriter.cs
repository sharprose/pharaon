namespace Gear.Files.Json
{
    using System;

    public interface IJsonFileWriter : ITypedFileService, IDisposable
    {
        void Write<T>(T data, string directory, string filename);
    }
}