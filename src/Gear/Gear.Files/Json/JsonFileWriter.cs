namespace Gear.Files.Json
{
    using System;

    using DI.Services;

    using Serialization.Services;

    public class JsonFileWriter : IJsonFileWriter
    {
        public JsonFileWriter(IContainer container)
        {
            this._serializer = container.GetInstance<IJsonSerializer>();
            this._writer = new TextWriter();
        }

        public void Write<T>(T data, string directory, string filename)
        {
            if (string.IsNullOrEmpty(directory) ||
                string.IsNullOrEmpty(filename) ||
                data == null)
            {
                throw new ArgumentException();
            }

            var value = this._serializer.Serialize(data);
            this._writer.Write(directory, $"{filename}.json", value);
        }

        public string FileType => "json";

        private IJsonSerializer _serializer;
        
        private IFileWriter _writer;

        private bool _isDisposed = false;
        
        public void Dispose()
        {
            if (!this._isDisposed)
            {
                this._serializer = null;
                this._writer = null;
                this._isDisposed = true;
            }
        }
    }
}