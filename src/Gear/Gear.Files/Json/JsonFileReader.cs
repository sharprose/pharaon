namespace Gear.Files.Json
{
    using System;

    using DI.Services;

    using Serialization.Services;

    public class JsonFileReader : IJsonFileReader
    {
        public JsonFileReader(IContainer contaienr)
        {
            this._serializer = contaienr.GetInstance<IJsonSerializer>();
            this._reader = new TextReader();
        }

        public string FileType => "json";

        private IJsonSerializer _serializer;
        
        private IFileReader _reader;

        private bool _isDisposed = false;
        
        public T Read<T>(string directory, string filename)
        {
            if (string.IsNullOrEmpty(directory) ||
                string.IsNullOrEmpty(filename))
            {
                throw new ArgumentException();
            }
            
            var value = this._reader.Read(directory, $"{filename}.json");
            
            T result = this._serializer.Deserialize<T>(value); 
            
            return result;
        }

        public void Dispose()
        {
            if (!this._isDisposed)
            {
                this._reader = null;
                this._serializer = null;

                this._isDisposed = true;
            }
        }
    }
}