namespace Gear.Files
{
    using DI.Services;

    using Json;

    using Moduling;

    public class Module : IModule
    {
        public string Code => "Gear.Files";

        public void Register(IContainer container)
        {
            container.Register<ITextReader, TextReader>();
            container.Register<ITextWriter, TextWriter>();
            container.Register<IFileResolver, FileResolver>();
        }
    }
}