namespace Gear.Files.Json
{
    public interface IFileResolver
    {
        void SaveToText<T>(T instance, string path, string filename) where T : class;
        
        T LoadFromText<T>(string path, string filename) where T : class;
    }
}