namespace Gear.Files.Json
{
    using System;

    public interface ITypedFileReader : ITypedFileService, IDisposable
    {
        T Read<T>(string directory, string filename);
    }
}