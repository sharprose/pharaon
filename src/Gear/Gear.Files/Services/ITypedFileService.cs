namespace Gear.Files.Json
{
    using System;

    public interface ITypedFileService
    {
        string FileType { get; }
    }
}