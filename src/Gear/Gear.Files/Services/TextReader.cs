namespace Gear.Files.Json
{
    using System.IO;
    using System.Text;
    using System.Threading.Tasks;

    public class TextReader : BaseReader, ITextReader
    {
        protected override async Task<string> InnerRead(string directory, string filename)
        {
            var path = $"{directory}\\{filename}";

            var stringBuilder = new StringBuilder();

            char[] chars;

            using (var reader = File.OpenText(path))
            {
                chars = new char[reader.BaseStream.Length];

                await reader.ReadAsync(chars, 0, (int) reader.BaseStream.Length);
            }

            foreach (var cha in chars)
            {
                stringBuilder.Append(cha);
            }

            var result = stringBuilder.ToString();

            return result;
        }
    }
}