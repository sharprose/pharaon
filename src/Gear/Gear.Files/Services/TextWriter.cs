namespace Gear.Files.Json
{
    using System.IO;
    using System.Threading.Tasks;

    public class TextWriter : BaseWriter, ITextWriter
    {
        protected override Task InnerWriter(string directory, string filename, string data)
        {
            var path = $"{directory}\\{filename}";

            using (var writer = File.CreateText(path))
            {
                return writer.WriteAsync(data);
            }
        }
    }
}