namespace Gear.Files.Json
{
    using DI.Services;
    using Serialization.Services;

    public class FileResolver : IFileResolver
    {
        public FileResolver(IContainer container)
        {
            _container = container;
        }

        private IContainer _container;
        
        public void SaveToText<T>(T instance, string path, string filename) where T : class
        {
            var serializer = _container.GetInstance<IJsonSerializer>();

            var value = serializer.Serialize(instance);

            using (var writer = new JsonFileWriter(_container))
            {
                 writer.Write(value, path, filename);
            }
        }

        public T LoadFromText<T>(string path, string filename) where T : class
        {
            var serializer = _container.GetInstance<IJsonSerializer>();
            
            var reader = new JsonFileReader(_container);
            var result = reader.Read<T>(path, filename);

            return result;
        }
    }
}