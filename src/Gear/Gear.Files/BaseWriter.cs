namespace Gear.Files
{
    using System;
    using System.IO;
    using System.Threading.Tasks;

    public abstract class BaseWriter : IFileWriter
    {
        public void Write(string directory, string filename, string data, bool rewrite = true)
        {
            var task = this.WriteAsync(directory, filename, data, rewrite);

            //task.Start();
            task.Wait();
        }

        public async Task WriteAsync(string directory, string filename, string data, bool rewrite = true)
        {
            this.BeforeWrite(directory, filename, rewrite);

            await this.InnerWriter(directory, filename, data);
        }

        protected abstract Task InnerWriter(string directory, string filename, string data);

        private void BeforeWrite(string directory, string filename, bool rewrite)
        {
            if (string.IsNullOrEmpty(directory) ||
                string.IsNullOrEmpty(filename))
            {
                throw new ArgumentException();
            }

            if (!Directory.Exists(directory))
            {
                throw new DirectoryNotFoundException(directory);
            }

            var path = $"{directory}\\{filename}";

            if (!File.Exists(path) &&
                !rewrite)
            {
                throw new FileNotFoundException(path);
            }
        }
    }
}