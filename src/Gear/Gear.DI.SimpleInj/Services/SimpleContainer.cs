namespace Gear.DI.SimpleInj.Services
{
    using System;
    using System.Collections.Generic;

    using DI.Services;

    using SimpleInjector;

    public sealed class SimpleContainer : IContainer
    {
        private bool _isDisposed;

        public SimpleContainer()
        {
            this.container = new Container();

            this.container.Options.DefaultLifestyle = Lifestyle.Transient;
            this.container.Options.AllowOverridingRegistrations = true;

            this.container.RegisterInstance<IContainer>(this);
        }

        private Container container { get; }

        public void Register<T>(ComponentLifestyleType lifestyleType = ComponentLifestyleType.Transient)
            where T : class
        {
            var lifestyle = this.GetLifestyle(lifestyleType);

            this.container.Register<T>(lifestyle);
        }

        public void Register<T, TImpl>(ComponentLifestyleType lifestyleType = ComponentLifestyleType.Transient)
            where T : class
            where TImpl : class, T
        {
            var lifestyle = this.GetLifestyle(lifestyleType);

            this.container.Register<T, TImpl>(lifestyle);
        }

        public void AddToCollection<T>(IEnumerable<Type> types,
            ComponentLifestyleType lifestyleType = ComponentLifestyleType.Transient)
            where T : class
        {
            var lifestyle = this.GetLifestyle(lifestyleType);

            foreach (var type in types)
            {
                this.container.Collection.Append(typeof(T), type, lifestyle);
            }
        }

        public T GetInstance<T>()
            where T : class
        {
            if (!this.HasRegistrations<T>())
            {
                return null;
            }

            return this.container.GetInstance<T>();
        }

        public IEnumerable<T> GetAllInstance<T>()
            where T : class
        {
            if (!this.HasRegistrations<IEnumerable<T>>())
            {
                return new T[] { };
            }

            return this.container.GetAllInstances<T>();
        }

        public bool Verify()
        {
            this.container.Verify();

            return this.container.IsVerifying;
        }

        public void Dispose()
        {
            if (!this._isDisposed)
            {
                this.container?.Dispose();
                this._isDisposed = true;
            }
        }

        private Lifestyle GetLifestyle(ComponentLifestyleType lifestyleType)
        {
            switch (lifestyleType)
            {
                case ComponentLifestyleType.Singleton:
                    return Lifestyle.Singleton;

                default:
                    return Lifestyle.Transient;
            }
        }

        private bool HasRegistrations<T>()
        {
            var types = this.container.GetRegistration(typeof(T));

            return types != null;
        }
    }
}