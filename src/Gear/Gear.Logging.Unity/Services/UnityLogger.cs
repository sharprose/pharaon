namespace Gear.Logging.Unity.Services
{
    using System;
    using DI.Services;
    using Logging.Services;
    using UnityEngine;

    public class UnityLogger : BaseLogWriter
    {
        public UnityLogger(IContainer container) : base(container)
        {
        }

        public override void WriteToLog(string message, MessageType messageType)
        {
            switch (messageType)
            {
                case MessageType.Error:
                    Debug.LogError(message);
                    break;
                case MessageType.Message:
                    Debug.Log(message);
                    break;
            }
        }
    }
}