namespace Gear.DI.SimpleInj.Tests.Models
{
    using System;

    public class ServiceAB : IServiceA
    {
        public void Do()
        {
            Console.WriteLine($"{typeof(ServiceAB).Name}");
        }
    }
}