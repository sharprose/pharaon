namespace Gear.DI.SimpleInj.Tests.Models
{
    using System;

    public class ServiceAA : IServiceA
    {
        public void Do()
        {
            Console.WriteLine($"{typeof(ServiceAA).Name}");
        }
    }
}