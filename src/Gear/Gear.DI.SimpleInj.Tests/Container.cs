﻿namespace Gear.DI.SimpleInj.Tests
{
    using Models;

    using NUnit.Framework;

    using Services;

    [TestFixture]
    public class Container
    {
        [Test]
        public void GetAllInstance()
        {
            var container = new SimpleContainer();

            container.AddToCollection<IServiceA>(new[]
            {
                typeof(ServiceAA),
                typeof(ServiceAB)
            });

            container.Verify();

            var services = container.GetAllInstance<IServiceA>();

            Assert.NotNull(services);
            Assert.IsNotEmpty(services);
        }

        [Test]
        public void GetInstance()
        {
            var container = new SimpleContainer();
            container.Register<IServiceA, ServiceAA>();
            container.Verify();

            var instance = container.GetInstance<IServiceA>();

            Assert.NotNull(instance);
            Assert.True(instance is IServiceA);
        }

        [Test]
        public void Registration()
        {
            var container = new SimpleContainer();
            container.Register<IServiceA, ServiceAA>();
            container.Register<ServiceAA>();

            container.Verify();
        }

        [Test]
        public void RegistrationCollection()
        {
            var container = new SimpleContainer();

            container.AddToCollection<IServiceA>(new[]
            {
                typeof(ServiceAA),
                typeof(ServiceAB)
            });

            container.Verify();
        }
    }
}