namespace Gear.Serialization.NS.Services
{
    using System.IO;
    using System.Text;
    using System.Xml;

    using DI.Services;

    using Newtonsoft.Json;

    using Serialization.Services;

    using Formatting = Newtonsoft.Json.Formatting;
    using NewtonsoftSerialize = Newtonsoft.Json.JsonSerializer;

    public class JsonSerializer : IJsonSerializer
    {
        public JsonSerializer(IContainer container)
        {
            var settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All,
                Formatting = Formatting.None,
                DateFormatString = "yyyy.MM.dd hh:mm:ss.f zzz"
            };

            var converters = container.GetAllInstance<JsonConverter>();

            foreach (var converter in converters)
            {
                settings.Converters.Add(converter);
            }

            this.Serializer = NewtonsoftSerialize.Create(settings);
        }

        private NewtonsoftSerialize Serializer { get; }

        public string Serialize(object data)
        {
            var result = new StringBuilder();

            using (var writer = new JsonTextWriter(new StringWriter(result)))
            {
                this.Serializer.Serialize(writer, data);
            }

            return result.ToString();
        }

        public T Deserialize<T>(string data)
        {
            using (var textReader = new StringReader(data))
            {
                using (var reader = new JsonTextReader(textReader))
                {
                    return this.Serializer.Deserialize<T>(reader);
                }
            }
        }

        public object Deserialize(string data)
        {
            using (var textReader = new StringReader(data))
            {
                using (var reader = new JsonTextReader(textReader))
                {
                    return this.Serializer.Deserialize(reader);
                }
            }
        }
    }
}