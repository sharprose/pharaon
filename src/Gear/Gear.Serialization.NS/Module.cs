namespace Gear.Serialization.NS
{
    using DI.Services;

    using Moduling;

    using Serialization.Services;

    using Services;

    public class Module : IModule
    {
        public string Code => "Gear.Serialization.NS (Newtonsoft)";

        public void Register(IContainer container)
        {
            container.Register<IJsonSerializer, JsonSerializer>();
        }
    }
}