namespace Gear.Moduling
{
    using DI.Services;

    public interface IModule
    {
        /// <summary>
        ///     Код модуля
        /// </summary>
        string Code { get; }

        /// <summary>
        ///     Регистрация типов
        /// </summary>
        /// <param name="container"></param>
        void Register(IContainer container);
    }
}