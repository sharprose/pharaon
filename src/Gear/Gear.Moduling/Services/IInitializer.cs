namespace Gear.Moduling.Services
{
    using DI.Services;

    public interface IInitializer
    {
        int Order { get; }

        void Initialize(IContainer container);
    }
}