#define PHARAON

namespace Gear.Moduling.Services
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    using DI.Services;

    public sealed class ModuleActivator
    {
        private readonly string additionalPath = string.Empty;

        public ModuleActivator(IContainer container, string additionalPath = "")
        {
            this.container = container;
            this.additionalPath = additionalPath;

            this.All = new List<IModule>(20);
            this.Activated = new List<IModule>(20);
            this.Initializers = new List<IInitializer>();
        }

        private IContainer container { get; }

        /// <summary>
        ///     Все найденные модули
        /// </summary>
        public List<IModule> All { get; }

        /// <summary>
        ///     Зарегистрованные модули
        /// </summary>
        public List<IModule> Activated { get; }

        public List<IInitializer> Initializers { get; }

        /// <summary>
        ///     Найти и зарегистрировать модули
        /// </summary>
        public void ActivateModules()
        {
            var assemblies = this.GetAssemblies();
            var initializers = new List<IInitializer>();

            foreach (var assembly in assemblies)
            {
                this.ProcessAssembly(assembly);
                initializers.AddRange(this.GetInitializers(assembly));
            }

            var sortedInitializers = initializers.OrderBy(x => x.Order);
            
            foreach (var initializer in sortedInitializers)
            {
                this.ProcessInitializer(initializer);
            }
            
            container.Verify();
        }

        private void ProcessAssembly(Assembly assembly)
        {
            var modules = this.GetModules(assembly);

            foreach (var module in modules)
            {
                this.All.Add(module);
            }

            foreach (var module in modules)
            {
                this.ProcessModule(module);
            }
        }

        private void ProcessModule(IModule module)
        {
            try
            {
                module.Register(this.container);

                this.Activated.Add(module);
            }
            catch (Exception e)
            {
                Console.WriteLine(module.Code);
                Console.WriteLine(e);
                throw;
            }
            

#if DEBUG

            //Debug.Log($"Module activated: {module.Code}");
#endif
        }

        private void ProcessInitializer(IInitializer initializer)
        {
            initializer.Initialize(this.container);

            this.Initializers.Add(initializer);
        }

        private IEnumerable<IModule> GetModules(Assembly assembly)
        {
            var result = new List<IModule>();

            var types = assembly.GetTypes();

            foreach (var type in types)
            {
                var interfaces = type.GetInterfaces();

                if (interfaces.Any(i => i == typeof(IModule)))
                {
                    var instance = this.CreateInstance(type);

                    result.Add(instance as IModule);
                }
            }

            return result;
        }

        private IEnumerable<IInitializer> GetInitializers(Assembly assembly)
        {
            var result = new List<IInitializer>();

            var types = assembly.GetTypes();

            foreach (var type in types)
            {
                var interfaces = type.GetInterfaces();

                if (interfaces.Any(i => i == typeof(IInitializer)))
                {
                    var instance = this.CreateInstance(type);

                    result.Add(instance as IInitializer);
                }
            }

            return result;
        }

        private object CreateInstance(Type type)
        {
            var instance = Activator.CreateInstance(type);

            return instance;
        }

        private List<Assembly> GetAssemblies()
        {
            var result = new List<Assembly>();

            var path = Path.GetDirectoryName(this.GetType().Assembly.Location); //Directory.GetCurrentDirectory();

            if (!string.IsNullOrEmpty(this.additionalPath))
            {
                path = $"{path}\\{this.additionalPath}";
            }

            foreach (var dll in Directory.GetFiles(path, "*.dll"))
            {
#if DEBUG
                //Debug.Log($"ModuleActivator (GetAssemblies) (LoadFrom): {dll}");
#endif
                var assembly = Assembly.LoadFrom(dll);

                result.Add(assembly);
            }

#if DEBUG

            //Debug.Log($"Find {result.Count} modules.");
#endif

            return result;
        }
    }
}