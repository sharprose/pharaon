namespace Gear.Storage
{
    using DI;
    using DI.Services;
    using Moduling;
    using Services;

    public class Module : IModule
    {
        public string Code => "Gear.Storage";
        
        public void Register(IContainer container)
        {
            container.Register<IStorageFactory, StorageFactory>(ComponentLifestyleType.Singleton);
            container.Register<IDynamicStorage, DynamicStorage>(ComponentLifestyleType.Singleton);
        }
    }
}