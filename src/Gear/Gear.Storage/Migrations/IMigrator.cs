namespace Gear.Storage.Migrations
{
    public interface IMigrator
    {
        void Migrate();
    }
}