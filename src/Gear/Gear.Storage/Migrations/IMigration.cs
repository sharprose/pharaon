namespace Gear.Storage.Migrations
{
    public interface IMigration
    {
        int Order { get; }

        void Execute();
    }
}