namespace Gear.Storage.Migrations
{
    using System.Collections.Generic;
    using System.Linq;
    using DI.Services;

    public class DefaultMigrator : IMigrator
    {
        public DefaultMigrator(IContainer container)
        {
            this._migrations = new List<IMigration>();
        }

        private List<IMigration> _migrations;
        
        public IEnumerable<IMigration> Migrations => _migrations;
        
        public void Migrate()
        {
            var orderedMigrations = Migrations.OrderBy(x => x.Order);
            
            foreach (var migration in orderedMigrations)
            {
                migration.Execute();
            }
        }

        public void Add(IMigration migration)
        {
            _migrations.Add(migration);
        }
    }
}