namespace Gear.Storage.Models
{
    using System;

    public sealed class Cell
    {
        public Cell(string value)
        {
            this._data = new Tuple<string, long, int, float>(value, default(long), default(int), default(float));
        }

        public Cell(int value)
        {
            this._data = new Tuple<string, long, int, float>(default(string), default(long), value, default(float));
        }
        
        public Cell(long value)
        {
            this._data = new Tuple<string, long, int, float>(default(string), value, default(int), default(float));
        }

        public Cell(float value)
        {
            this._data = new Tuple<string, long, int, float>(default(string), default(int), default(int), value);
        }
        
        public Type ValueType = null;

        public Tuple<string, long, int, float> _data = null;

        public string AsString()
        {
            return this._data.Item1;
        }
        
        public long AsLong()
        {
            return this._data.Item2;
        }
        
        public int AsInteger()
        {
            return this._data.Item3;
        }
    }
}