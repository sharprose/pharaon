namespace Gear.Storage.Models
{
    using System.Collections.Generic;

    public class DynamicRow
    {
        private readonly Dictionary<string, Cell> _storage =
            new Dictionary<string, Cell>(10);

        public Cell this[string key] => Get(key);

        public Cell Get(string key)
        {
            var normalizeKey = NormalizeKey(key);

            return _storage[normalizeKey];
        }

        public void Set(string key, string value)
        {
            var normalKey = OnBeforeAdd(key);

            _storage[normalKey] = new Cell(value);
        }

        public void Set(string key, float value)
        {
            var normalKey = OnBeforeAdd(key);

            _storage[normalKey] = new Cell(value);
        }
        
        public void Set(string key, int value)
        {
            var normalKey = OnBeforeAdd(key);

            _storage[normalKey] = new Cell(value);
        }

        public void Set(string key, long value)
        {
            var normalKey = OnBeforeAdd(key);

            _storage[normalKey] = new Cell(value);
        }

        private string OnBeforeAdd(string key)
        {
            var normalKey = NormalizeKey(key);

            if (!_storage.ContainsKey(normalKey))
            {
                _storage.Add(normalKey, null);
            }

            return normalKey;
        }

        private string NormalizeKey(string key)
        {
            var result = key.ToLower();

            return result;
        }
    }
}