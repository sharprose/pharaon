namespace Gear.Storage
{
    using DI.Services;

    using Entities;

    using Services;

    public static class ContainerExtensions
    {
        public static void RegisterStorage<T>(this IContainer container)
            where T : class, IEntity
        {
            var storageFactory = container.GetInstance<IStorageFactory>();
            storageFactory.Register<T>();
        }
    }
}