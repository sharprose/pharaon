namespace Gear.Storage.Entities
{
    using System.Reflection;

    public static class Extentions
    {
        public static bool IsNew(this IEntity entity)
        {
            return entity.Id == default(long);
        }

        public static StorageType GetStorageType(this IEntity entity)
        {
            var attribute = entity.GetType().GetCustomAttribute(typeof(EntityDescriptionAttribute)) as EntityDescriptionAttribute;

            if (attribute != null)
            {
                return attribute.StorageType;
            }

            return StorageType.File;
        }
    }
}