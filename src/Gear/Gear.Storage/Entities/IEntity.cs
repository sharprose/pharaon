namespace Gear.Storage.Entities
{
    public interface IEntity
    {
        long Id { get; set; }
    }
}