namespace Gear.Storage.Entities
{
    public abstract class BaseEntity : IEntity
    {
        public long Id { get; set; }
    }
}