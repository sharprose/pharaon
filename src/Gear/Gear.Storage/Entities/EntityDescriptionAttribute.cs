namespace Gear.Storage.Entities
{
    using System;

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class EntityDescriptionAttribute : Attribute
    {
        public StorageType StorageType = StorageType.File;
    }
}