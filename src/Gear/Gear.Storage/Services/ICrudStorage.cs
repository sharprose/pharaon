namespace Gear.Storage.Services
{
    using System.Collections.Generic;
    using Entities;

    public interface ICrudStorage : IStorage
    {
        void Save<T>(T entity) where T : class, IEntity;
        
        void Save<T>(IEnumerable<T> entities) where T : class, IEntity;

        T Get<T>(long id) where T : class, IEntity;
        
        IEnumerable<T> Get<T>(params long[] ids) where T : class, IEntity;
        
        IEnumerable<T> GetAll<T>() where T : class, IEntity;

        void Delete(params long[] ids);
    }
}