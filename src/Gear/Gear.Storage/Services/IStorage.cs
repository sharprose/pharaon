namespace Gear.Storage.Services
{
    using Entities;

    public interface IStorage
    {
        StorageType Type { get; }

        void Initialize<T>() where T : class, IEntity;
    }
}