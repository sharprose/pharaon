namespace Gear.Storage.Services
{
    using Models;

    public interface IDynamicStorage 
    {
        DynamicRow this[string key] { get; }
    }
}