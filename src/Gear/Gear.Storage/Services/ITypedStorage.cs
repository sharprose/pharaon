namespace Gear.Storage.Services
{
    public interface ITypedStorage
    {
        T Get<T>() where T : class;

        void Save<T>(T instance) where T : class;
    }
}