namespace Gear.Storage.Services
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using DI.Services;
    using Entities;
    using Storages;

    public class StorageFactory : IStorageFactory
    {
        public StorageFactory(IContainer container)
        {
            this._container = container;
        }

        private IContainer _container;
        
        private Dictionary<Type, ICrudStorage> _crudStorages = new Dictionary<Type, ICrudStorage>(10);
        
        public void Register<T>() where T : class, IEntity
        {
            var type = typeof(T);
            
            if (this._crudStorages.ContainsKey(type))
            {
                throw new Exception($"Type for entity (\"{type.Name}\") registered.");
            }
            
            var description = type.GetCustomAttribute<EntityDescriptionAttribute>();

            StorageType storageType = StorageType.File;
            ICrudStorage storage;
            
            if (description != null)
            {
                storageType = description.StorageType;
            }

            switch (storageType)
            {
                case StorageType.InMemmory:
                    storage = this._container.GetInstance<IInMemmoryStorage>();
                    break;
                case StorageType.Relation:
                    storage = this._container.GetInstance<IRelationStorage>();
                    break;
                default:
                    storage = this._container.GetInstance<IFileStorage>();
                    break;
            }

            if (storage == null)
            {
                throw new Exception($"Storage for {type.Name} not found.");
            }
            
            storage.Initialize<T>();
            
            this._crudStorages.Add(type, storage);
        }

        public ICrudStorage Get<T>() where T : class, IEntity
        {
            var type = typeof(T);

            if (this._crudStorages.ContainsKey(type))
            {
                return this._crudStorages[type];
            }
            
            throw new Exception($"Crud storage for {type.Name} not found.");
        }
    }
}