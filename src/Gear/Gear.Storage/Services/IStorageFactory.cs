namespace Gear.Storage.Services
{
    using Entities;

    public interface IStorageFactory
    {
        void Register<T>() where T : class, IEntity;
        
        ICrudStorage Get<T>() where T : class, IEntity;
    }
}