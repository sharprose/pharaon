namespace Gear.Storage.Services
{
    using System.Collections.Generic;
    using DI.Services;
    using Models;

    public class DynamicStorage : IDynamicStorage
    {
        private static readonly Dictionary<string, DynamicRow> _storage = new Dictionary<string, DynamicRow>(10);

        public DynamicStorage(IContainer container)
        {
        }

        private IContainer _container { get; }

        public DynamicRow this[string key]
        {
            get
            {
                var key_low = key.ToLower();

                if (!_storage.ContainsKey(key_low))
                {
                    _storage.Add(key_low, new DynamicRow());
                }

                return _storage[key_low];
            }
        }
    }
}