namespace Gear.Storage.Services
{
    using System;
    using System.Collections.Generic;

    using DI.Services;

    using Serialization.Services;

    public class TypedStorage : ITypedStorage
    {
        private Dictionary<string, object> _storage = new Dictionary<string, object>(20);
            
        public TypedStorage(IContainer container)
        {
            var serializer = container.GetInstance<IJsonSerializer>();
            
        }

        public T Get<T>()
            where T : class
        {
            var type = typeof(T);
            var name = this.ExtractName(type);

            if (this._storage.ContainsKey(name))
            {
                return this._storage[name] as T;
            }
            
            return default(T);
        }

        public void Save<T>(T instance)
            where T : class
        {
            var type = typeof(T);
            var name = this.ExtractName(type);
            
            if (!this._storage.ContainsKey(name))
            {
                this._storage.Add(name, instance);
            }
            else
            {
                this._storage[name] = instance;    
            }
        }

        private string ExtractName(Type type)
        {
            var name = type.FullName.ToLower();

            return name;
        }
    }
}