namespace Gear.Logging
{
    using DI.Services;
    using Moduling;
    using Services;

    public class Module : IModule
    {
        public string Code => "Gear.Logging";
        
        public void Register(IContainer container)
        {
            container.Register<ILogger, Logger>();
        }
    }
}