namespace Gear.Logging
{
    using DI.Services;
    using Services;

    public static class ConainerExtenction
    {
        public static void AddLogger<T>(this IContainer container) where T : class, ILogWriter
        {
            var logger = container.GetInstance<ILogger>();
            
            logger.AppendWriter<T>();
        }
    }
}