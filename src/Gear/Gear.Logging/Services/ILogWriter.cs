namespace Gear.Logging.Services
{
    using System;

    public interface ILogWriter
    {
        void Message(string message);

        void Error(Exception exception);
    }
}