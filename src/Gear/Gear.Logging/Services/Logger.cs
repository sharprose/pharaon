namespace Gear.Logging.Services
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using DI.Services;

    public class Logger : ILogger
    {
        public Logger(IContainer container)
        {
            _writers = new List<ILogWriter>(5);
            _container = container;
        }
        
        private List<ILogWriter> _writers;

        private IContainer _container;
        
        public void AppendWriter<T>() where T : class, ILogWriter
        {
            var instance = Activator.CreateInstance(typeof(T),new object[ ]{_container}) as ILogWriter;
            
            _writers.Add(instance);
        }

        public void Message(string message)
        {
            foreach (var writer in _writers)
            {
                writer.Message(message);
            }
        }

        public void Error(Exception exception)
        {
            foreach (var writer in _writers)
            {
                writer.Error(exception);
            }
        }
    }
}