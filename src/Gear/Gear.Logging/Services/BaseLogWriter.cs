namespace Gear.Logging.Services
{
    using System;
    using DI.Services;

    public abstract class BaseLogWriter : ILogWriter
    {
        public BaseLogWriter(IContainer container)
        {
            
        }
        
        public void Message(string message)
        {
            var record = $"{GetTimestamp()} Message: {message}";

            WriteToLog(record, MessageType.Message);
        }

        public void Error(Exception exception)
        {
            var record = $"{GetTimestamp()} Exception: {exception.Message}\n{exception.StackTrace}";

            WriteToLog(record, MessageType.Error);
        }

        private string GetTimestamp()
        {
            var result = DateTime.Now.ToString("hh:mm:ss t z");

            return result;
        }

        public abstract void WriteToLog(string message, MessageType messageType);

    }
}