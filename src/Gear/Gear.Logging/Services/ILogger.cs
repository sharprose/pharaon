namespace Gear.Logging.Services
{
    using System;

    public interface ILogger
    {
        void AppendWriter<T>() where T : class, ILogWriter;
        
        void Message(string message);

        void Error(Exception exception);
    }
}