namespace Gear.Logging.TextFile.Services
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using Configuration.Configurations;
    using Configuration.Services;
    using DI.Services;
    using Logging.Services;

    public class TextLogger : BaseLogWriter
    {
        private const string _fileName = "logfile.log";

        private readonly string _path;

        private readonly long MaxSize = 1024L;

        public TextLogger(IContainer container) : base(container)
        {
            var configurationManager = container.GetInstance<IConfigurationService>();
            var appCfg = configurationManager.Get<ApplicationConfig>();

            _path = appCfg.RootPath;
        }

        public string DirectoryPath => $"{_path}\\logs";

        public string FilePath => $"{DirectoryPath}\\{_fileName}";

        public override void WriteToLog(string message, MessageType messageType)
        {
            Task.Run(() =>
            {
                using (var textFile = ResolveWriter())
                {
                    textFile.Write(message);
                }
            }).Wait();
        }

        private StreamWriter ResolveWriter()
        {
            if (!Directory.Exists(DirectoryPath))
            {
                Directory.CreateDirectory(DirectoryPath);
            }

            var result = File.CreateText(FilePath);

            if (result.BaseStream.Length > MaxSize)
            {
                result.Flush();
                result.Close();

                var newName = $"logfile_{DateTime.Now.Ticks}.log";
                File.Move(FilePath, newName);

                result = File.CreateText(FilePath);
            }

            return result;
        }
    }
}