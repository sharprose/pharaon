namespace Gear.Logging.TextFile
{
    using DI.Services;
    using Moduling.Services;
    using Services;

    public class Initializer : IInitializer
    {
        public int Order => 4;
        
        public void Initialize(IContainer container)
        {
            container.AddLogger<TextLogger>();
        }
    }
}