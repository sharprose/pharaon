namespace Gear.Storage.File.Services
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using DI.Services;

    using Files.Json;

    public class Indexer : IDisposable
    {
        private List<long> _indexes = new List<long>(100);

        public IEnumerable<long> Items => this._indexes;

        private bool _disposed = false;

        private IContainer _container;

        private string _path;
        
        public Indexer(IContainer container, string path)
        {
            this._container = container;
            this._path = path;
            
            this.Load();
        }

        public long Next()
        {
            long current = 0;

            while (this._indexes.Contains(current))
            {
                current++;
            }
            
            this._indexes.Add(current);

            return current;
        }

        public void Remove(long value)
        {
            if (this._indexes.Contains(value))
            {
                this._indexes.Remove(value);
            }
        }

        public void Dispose()
        {
            if (!this._disposed)
            {
                this.Save();
                
                this._disposed = true;
            }
        }

        private void Save()
        {
            new JsonFileWriter(this._container).Write(this._indexes, this._path, "idxs");
        }
        
        private void Load()
        {
            var reader = new JsonFileReader(this._container);
            var idxPath = $"{this._path}\\idxs";

            if (File.Exists(idxPath))
            {
                var record = reader.Read<List<long>>(this._path, "idxs");

                this._indexes = record;
            }
        }
    }
}