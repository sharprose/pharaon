namespace Gear.Storage.File.Services
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Configuration.Configurations;
    using Configuration.Services;
    using DI.Services;

    using Entities;

    using Files.Json;

    using Storage.Services;
    using Storages;
    
    public class FileStorage : IFileStorage
    {
        public FileStorage(IContainer container)
        {
            var ds = container.GetInstance<IDynamicStorage>();
            var configurationManager = container.GetInstance<IConfigurationService>();
            var appCfg = configurationManager.Get<ApplicationConfig>();
            
            this._container = container;
            this._rootDirectory = $"appCfg.RootPath\\{fsCatalogName}";
            this._writer = new JsonFileWriter(container);
            this._reader = new JsonFileReader(container);
        }

        ~FileStorage()
        {
            this._indexer.Dispose();
            this._writer.Dispose();
        }

        private const string fsCatalogName = ".fs";
        
        private IContainer _container;
        
        private string _rootDirectory;

        private string _fullDirectory;

        private Indexer _indexer;

        private JsonFileWriter _writer;
        
        private JsonFileReader _reader;
        
        public StorageType Type => StorageType.File;
        
        public void Initialize<T>() where T : class, IEntity
        {
            var type = typeof(T);

            this._fullDirectory = $"{this._rootDirectory}\\{type.Name}";

            if (!Directory.Exists(this._fullDirectory))
            {
                Directory.CreateDirectory(this._fullDirectory);
            }
            
            this._indexer = new Indexer(this._container, this._fullDirectory);
        }

        public void Save<T>(T entity)
            where T : class, IEntity
        {
            this.Save<T>(new [] { entity });
        }

        public void Save<T>(IEnumerable<T> entities)
            where T : class, IEntity
        {
            foreach (var entity in entities)
            {
                if (entity.IsNew())
                {
                    entity.Id = this._indexer.Next();
                }

                var name = entity.Id.ToString();
                var path = $"{this._fullDirectory}\\{name}\\";

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                
                this._writer.Write(entity, path, name);
            }
        }

        public T Get<T>(long id)
            where T : class, IEntity
        {
            if (!this._indexer.Items.Contains(id))
            {
                return default(T);
            }

            var name = id.ToString();
            var path = $"{this._fullDirectory}\\{name}\\";
            
            var result = this._reader.Read<T>(path, name);

            return result;
        }

        public IEnumerable<T> Get<T>(params long[] ids)
            where T : class, IEntity
        {
            foreach (var id in ids)
            {
                yield return this.Get<T>(id);
            }
        }

        public IEnumerable<T> GetAll<T>()
            where T : class, IEntity
        {
            foreach (var id in this._indexer.Items)
            {
                yield return this.Get<T>(id);
            }
        }

        public void Delete(params long[] ids)
        {
            foreach (var id in ids)
            {
                var path = $"{this._fullDirectory}\\{id.ToString()}";

                if (Directory.Exists(path))
                {
                    this._indexer.Remove(id);
                    Directory.Delete(path, true);
                }
            }
        }
    }
}