namespace Gear.Storage.File
{
    using DI.Services;

    using Moduling;

    using Services;

    using Storages;

    public class Module : IModule
    {
        public string Code => "Gear.Storage.File";

        public void Register(IContainer container)
        {
            container.Register<IFileStorage, FileStorage>();
        }
    }
}