namespace Gear.DI.Services
{
    using System;
    using System.Collections.Generic;

    public interface IContainer : IDisposable
    {
        void Register<T>(ComponentLifestyleType lifestyleType = ComponentLifestyleType.Transient)
            where T : class;

        /// <summary>
        ///     Зарегистрировать тип или перезаписать ранее зарегистрированные реализации
        /// </summary>
        /// <param name="lifestyleType"></param>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TImpl"></typeparam>
        void Register<T, TImpl>(ComponentLifestyleType lifestyleType = ComponentLifestyleType.Transient)
            where T : class
            where TImpl : class, T;

        /// <summary>
        ///     Зарегистрировать типы реализации
        /// </summary>
        /// <param name="types"></param>
        /// <param name="lifestyleType"></param>
        /// <typeparam name="T"></typeparam>
        void AddToCollection<T>(IEnumerable<Type> types,
            ComponentLifestyleType lifestyleType = ComponentLifestyleType.Transient)
            where T : class;

        /// <summary>
        ///     Получить инстанс указаного типа
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T GetInstance<T>()
            where T : class;

        /// <summary>
        ///     Получить все инстансы указаного типа
        /// </summary>
        /// <typeparam name="T">Тип компонента</typeparam>
        /// <returns>Инстанс компонента</returns>
        IEnumerable<T> GetAllInstance<T>()
            where T : class;

        bool Verify();
    }
}