namespace Gear.DI
{
    public enum ComponentLifestyleType
    {
        Transient,

        Singleton
    }
}