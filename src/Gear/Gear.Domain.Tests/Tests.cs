﻿using System;

using NUnit.Framework;

namespace Gear.Domain.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using Entities;

    using Pharaon.Test;

    using Services;

    [TestFixture]
    public class Tests : BaseTest
    {
        [Test]
        [TestCase(100)]
        [TestCase(1000)]
        //[TestCase(10000)]
        public void Save(int count)
        {
            var dpf = this.Container.GetInstance<IDataProviderFactory>();
            var udp = dpf.Get<User>();

            var list = new List<User>();
            
            for (int i = 0; i < count; i++)
            {
                list.Add(new User()
                {
                    Login = i.ToString()
                });
            }
            
            StopwatchStart();
            udp.Save(list);
            StopwatchStop();
            
            Assert.True(true);
        }

        [Test]
        [TestCase(100)]
        [TestCase(1000)]
        //[TestCase(10000)]
        public void Get(int count)
        {
            var dpf = this.Container.GetInstance<IDataProviderFactory>();
            var udp = dpf.Get<Role>();
            
            var list = new List<Role>();
            
            for (int i = 0; i < count; i++)
            {
                list.Add(new Role()
                {
                    Name = i.ToString()
                });
            }

            udp.Save(list);
                
            StopwatchStart();
            var all = udp.GetAll();
            StopwatchStop();
        }

        [Test]
        [TestCase(100)]
        [TestCase(1000)]
        //[TestCase(10000)]
        public void Delete(int count)
        {
            var dpf = this.Container.GetInstance<IDataProviderFactory>();
            var udp = dpf.Get<Role>();
            
            var list = new List<Role>();
            
            for (int i = 0; i < count; i++)
            {
                list.Add(new Role()
                {
                    Name = i.ToString()
                });
            }
            
            udp.Save(list);

            var ids = list.Select(x => x.Id).ToArray();

            StopwatchStart();
            udp.Delete(ids);
            StopwatchStop();
        }
    }
}