namespace Gear.Domain.Tests.Entities
{
    using Storage.Entities;

    public class User : BaseEntity
    {
        public string Login { get; set; }
    }
}