namespace Gear.Domain.Tests.Entities
{
    using Storage.Entities;

    public class Role : BaseEntity
    {
        public string Name { get; set; }
    }
}