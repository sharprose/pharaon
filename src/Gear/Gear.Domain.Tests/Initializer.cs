namespace Gear.Domain.Tests
{
    using DI.Services;

    using Entities;

    using Moduling.Services;

    public class Initializer : IInitializer
    {
        public int Order => 1;

        public void Initialize(IContainer container)
        {
            container.RegisterEntity<User>();
            container.RegisterEntity<Role>();
        }
    }
}