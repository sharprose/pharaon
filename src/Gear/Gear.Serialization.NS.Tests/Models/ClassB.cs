namespace Gear.Serialization.NS.Tests.Models
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class ClassB
    {
        public ClassB()
        {
            this.ClassA = new ClassA();
            this.ListStr = new List<string>();
            this.DictionaryStrInt = new Dictionary<string, int>();
        }

        public ClassA ClassA { get; set; }

        public Dictionary<string, int> DictionaryStrInt { get; set; }

        public List<string> ListStr { get; set; }

        public void Load()
        {
            this.ListStr.Add("abc");
            this.ListStr.Add("def");
            this.ListStr.Add("ghj");

            this.DictionaryStrInt.Add("abc", 0);
            this.DictionaryStrInt.Add("def", 1);
            this.DictionaryStrInt.Add("ghj", 2);
        }
    }
}