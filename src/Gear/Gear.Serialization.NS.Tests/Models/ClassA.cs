namespace Gear.Serialization.NS.Tests.Models
{
    using System;

    [Serializable]
    public class ClassA
    {
        public byte Byte = 0;

        public char Char = 'a';

        public char[] CharArr = new char[3] {'a', 'b', 'c'};

        public DateTime DateTime = DateTime.Now;

        public DateTimeOffset DateTimeOffSet = DateTimeOffset.Now;

        public decimal Decimal = 566484213;

        public int Int = 132;

        public long Long = 321;

        public string Str = "abc";
    }
}