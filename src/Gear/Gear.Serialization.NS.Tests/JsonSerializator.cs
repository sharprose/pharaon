﻿namespace Gear.Serialization.NS.Tests
{
    using System;
    using System.Collections.Generic;

    using DI.SimpleInj.Services;

    using Models;

    using NUnit.Framework;

    using Services;

    [TestFixture]
    public class JsonSerializator
    {
        [Test]
        public void Deserialize()
        {
            var serializer = new JsonSerializer(new SimpleContainer());

            var str =
                "{ \"$type\": \"Gear.Serialization.NS.Tests.Models.ClassA, Gear.Serialization.NS.Tests\", \"Int\": 132, \"Long\": 321, \"Decimal\": 566484213.0, \"Char\": \"a\", \"CharArr\": { \"$type\": \"System.Char[], mscorlib\", \"$values\": [ \"a\", \"b\", \"c\" ] }, \"Str\": \"abc\", \"Byte\": 0, \"DateTime\": \"2019.06.14 04:32:50.3 +03:00\", \"DateTimeOffSet\": \"2019.06.14 04:32:50.3 +03:00\" }";

            var obj = serializer.Deserialize<ClassA>(str);

            Assert.NotNull(obj);
            Assert.True(obj.GetType() == typeof(ClassA));
            Assert.AreEqual(obj.Str, "abc");
        }

        [Test]
        [TestCase(typeof(List<string>),
            "{ \"$type\": \"System.Collections.Generic.List`1[[System.String, mscorlib]], mscorlib\", \"$values\": [ \"abc\", \"def\", \"ghj\" ] }")]
        [TestCase(typeof(ClassA),
            "{ \"$type\": \"Gear.Serialization.NS.Tests.Models.ClassA, Gear.Serialization.NS.Tests\", \"Int\": 132, \"Long\": 321, \"Decimal\": 566484213.0, \"Char\": \"a\", \"CharArr\": { \"$type\": \"System.Char[], mscorlib\", \"$values\": [ \"a\", \"b\", \"c\" ] }, \"Str\": \"abc\", \"Byte\": 0, \"DateTime\": \"2019.06.14 04:32:50.3 +03:00\", \"DateTimeOffSet\": \"2019.06.14 04:32:50.3 +03:00\" }")]
        public void DeserializeType(Type type, string value)
        {
            var serializer = new JsonSerializer(new SimpleContainer());
            var obj = serializer.Deserialize(value);

            Assert.NotNull(obj);
            Assert.True(obj.GetType() == type);
        }

        [Test]
        public void Serialize()
        {
            var obj = new ClassB();
            obj.Load();

            var serializer = new JsonSerializer(new SimpleContainer());

            var result = serializer.Serialize(obj);

            Assert.NotNull(result);
            Assert.NotZero(result.Length);
            Assert.False(string.IsNullOrEmpty(result));

            Console.WriteLine(result);
        }
    }
}