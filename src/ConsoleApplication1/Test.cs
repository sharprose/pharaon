namespace ConsoleApplication1
{
    using System;

    using Anklave.ECS.Components;
    using Anklave.ECS.Entities;
    using Anklave.ECS.Limousine.Systems;

    public class TestComponentA : IEcsComponent
    {
        public int A { get; set; }
    }
    
    public class TestSystemA : AbstractEcsSystem<TestComponentA>
    {
        public override void Execute(IEcsEntity entity, TestComponentA component)
        {
            if (component.A <= Int32.MaxValue)
            {
                component.A++;   
            }
        }
    }
    
    public class TestComponentB : IEcsComponent
    {
        public int A { get; set; }
    }
    
    public class TestSystemB : AbstractEcsSystem<TestComponentB>
    {
        public override void Execute(IEcsEntity entity, TestComponentB component)
        {
            if (component.A <= Int32.MaxValue)
            {
                component.A++;   
            }
        }
    }
}