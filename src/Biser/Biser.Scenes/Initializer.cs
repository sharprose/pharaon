namespace Biser.Scenes
{
    using Elder.Scene.Entities;
    using Gear.DI.Services;
    using Gear.Domain.Services;
    using Gear.Moduling.Services;

    public class Initializer : IInitializer
    {
        public int Order => 2;

        public void Initialize(IContainer container)
        {
            var factory = container.GetInstance<IDataProviderFactory>();
            var sdp = factory.Get<SceneDescription>();

            sdp.Save(new SceneDescription
            {
                Name = "mainemenu",
                Path = "MainMenu"
            });

            sdp.Save(new SceneDescription
            {
                Name = "game",
                Path = "Game"
            });
            
            sdp.Save(new SceneDescription
            {
                Name = "load",
                Path = "Load"
            });
        }
    }
}