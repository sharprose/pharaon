namespace Biser.Scenes
{
    using Gear.DI.Services;
    using Gear.Domain;
    using Gear.Moduling;

    public class Module : IModule
    {
        public string Code => "Biser.Scenes";
        
        public void Register(IContainer container)
        {
            
        }
    }
}