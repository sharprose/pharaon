namespace Anklave.ECS.Limousine
{
    public static class Constants
    {
        public const int DEFAULT_IDENTIFY = 0;
        
        public const int DEFAULT_COLLECTION_SIZE = 1024;

        public const int DEFAULT_ENTITIES_COUNT_PER_THREAD = 100;

        public const int DEFAULT_COMPONENTS_COUNT = 15;
    }
}