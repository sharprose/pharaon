namespace Anklave.ECS.Limousine.Entities
{
    using System;
    using System.Collections.Generic;

    using Common;

    using Components;

    using ECS.Components;
    using ECS.Entities;

    public sealed class EcsEntity : IdentityObject, IEcsEntity
    {
        private readonly Dictionary<Type, IEcsComponent> _components;

        public EcsEntity(int componentsCount)
        {
            this._components = new Dictionary<Type, IEcsComponent>(componentsCount);
        }

        public void Dispose()
        {
            this.OnRemove?.Invoke(this);

            foreach (var cmp in this._components)
            {
                this._components.Remove(cmp.Key);
                this.OnComponent?.Invoke(ComponentEventType.Remove, this, cmp.Value);
            }

            this.OnRemove = null;
        }

        public IEcsEntity AddComponent<T>() where T : class, IEcsComponent, new()
        {
            var instance = new T();

            this.AddComponent(instance);

            return this;
        }

        public IEcsEntity AddComponent<T>(T component)
            where T : class, IEcsComponent
        {
            var key = typeof(T);

            if (this._components.ContainsKey(key))
            {
                throw new Exception(
                    $"Component already exists. Entity {this.Id.ToString()}, Component type: {key.FullName}");
            }

            this._components.Add(key, component);

            this.OnComponent?.Invoke(ComponentEventType.Add, this, component);

            return this;
        }

        public void RemoveComponent<T>()
            where T : class, IEcsComponent
        {
            IEcsComponent component = null;

            this.OnComponent?.Invoke(ComponentEventType.Remove, this, component);
        }

        public T GetComponent<T>()
            where T : class, IEcsComponent
        {
            var key = typeof(T);

            if (this._components.ContainsKey(key))
            {
                return this._components[key] as T;
            }

            return null;
        }

        public event OnEntityRemoveEnventHandler OnRemove;

        public event OnAddComponentEventHandler OnComponent;
    }
}