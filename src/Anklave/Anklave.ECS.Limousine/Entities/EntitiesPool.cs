namespace Anklave.ECS.Limousine.Entities
{
    using System;
    using System.Collections.Generic;

    using ECS.Common;

    public class EntitiesPool : IPool<EcsEntity>
    {
        private Dictionary<int, EcsEntity> _items;

        public IEnumerable<EcsEntity> Items => this._items.Values;

        private int _size = 0;
        
        public int Size { get; }

        private object lockObj = new object();
        
        public EntitiesPool(int defaultPoolSize = Constants.DEFAULT_COLLECTION_SIZE)
        {
            this._items = new Dictionary<int, EcsEntity>(defaultPoolSize);
            this._size = defaultPoolSize;
        }
        
        public EcsEntity Get(int id)
        {
            if (this._items.ContainsKey(id))
            {
                return this._items[id];
            }

            return null;
        }

        public void Add(EcsEntity item)
        {
            if (this._items.Count >= this._size)
            {
                throw new Exception($"Max pool size ({this._size.ToString()}).");
            }
            
            var id = 0;

            while (this._items.ContainsKey(id))
            {
                id++;
            }
            
            item.SetId(id);

            lock (this.lockObj)
            {
                this._items.Add(id, item);   
            }
        }

        public void Remove(int id)
        {
            if (this._items.ContainsKey(id))
            {
                lock (this.lockObj)
                {
                    this._items.Remove(id);   
                }
            }
        }

        public void Clearout()
        {
            throw new NotImplementedException();
        }
    }
}