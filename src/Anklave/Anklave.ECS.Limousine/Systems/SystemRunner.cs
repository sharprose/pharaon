namespace Anklave.ECS.Limousine.Systems
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;

    using ECS.Entities;
    using ECS.Systems;

    public class SystemRunner
    {
        public void Run(IEnumerable<IEcsEntity> entities, IEcsSystem system)
        {
            this.InnerRun(entities, system);
        }

        private void InnerRun(IEnumerable<IEcsEntity> entities, IEcsSystem system)
        {
            Action action = () =>
            {
                foreach (var entity in entities)
                {
                    system.Execute(entity);
                }
            };
            
            var task = Task.Factory.StartNew(action);

            task.Wait();
        }
    }
}