namespace Anklave.ECS.Limousine.Systems
{
    using System.Collections.Generic;

    using ECS.Entities;
    using ECS.Systems;

    public class TaskRunParameters
    {
        public TaskRunParameters(IEnumerable<IEcsEntity> entities, IEcsSystem system, int page)
        {
            this._entities = entities;
            this._system = system;
            this._page = page;
        }

        private IEnumerable<IEcsEntity> _entities;
        private IEcsSystem _system;
        private int _page = 0;

        public int Page => this._page;
        
        public IEnumerable<IEcsEntity> Entities => this._entities;
        
        public IEcsSystem System => this._system;
    }
}