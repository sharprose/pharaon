namespace Anklave.ECS.Limousine.Systems
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;

    using Components;

    using ECS.Common;
    using ECS.Components;
    using ECS.Entities;
    using ECS.Systems;

    using Entities;

    using Services;

    public class SystemMap
    {
        private readonly Dictionary<Type, SystemEntitiesNode> _nodes;

        private readonly IPool<EcsEntity> _entityPool;

        private int _entitiesPerTask = 0; 
        
        public SystemMap(IPool<EcsEntity> entityPool, EcsSettings settings)
        {
            this._entityPool = entityPool;
            this._nodes = new Dictionary<Type, SystemEntitiesNode>(50);
            this._entitiesPerTask = settings.EntitiesPerThread;
        }

        public void Register<TS, TC>()
            where TS : class, IEcsSystem<TC>, new()
            where TC : class, IEcsComponent
        {
            var instance = new TS();
            var node = new SystemEntitiesNode(instance, this._entityPool, this._entitiesPerTask);
            var key = typeof(TC);
            
            this._nodes.Add(key, node); 
        }

        public void Push<T>(IEcsEntity entity, T component)
            where T : class, IEcsComponent
        {
            var key = component.GetType();
            
            if (this._nodes.ContainsKey(key))
            {
                this._nodes[key].Push(entity.Id);
            }
        }
        
        public void Execute()
        {
            foreach (var node in this._nodes)
            {
                node.Value.Run();
            }
        }

        public void Popup<T>(int entityId, T component)
            where T : class, IEcsComponent
        {
            var key = component.GetType();

            if (this._nodes.ContainsKey(key))
            {
                this._nodes[key].Remove(entityId);
            }
        }
    }
}