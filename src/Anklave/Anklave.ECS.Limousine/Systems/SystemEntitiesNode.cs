namespace Anklave.ECS.Limousine.Systems
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using ECS.Common;
    using ECS.Systems;

    using Entities;

    public class SystemEntitiesNode
    {
        public readonly List<int> _entities;

        private readonly int _entitiesPerTask = Constants.DEFAULT_ENTITIES_COUNT_PER_THREAD;

        private readonly IPool<EcsEntity> _entityPool;

        private readonly IEcsSystem _system;

        public SystemEntitiesNode(IEcsSystem system, IPool<EcsEntity> entityPool, int entitiesPerTask)
        {
            this._entitiesPerTask = entitiesPerTask;
            this._system = system;
            this._entityPool = entityPool;
            this._entities = new List<int>(this._entityPool.Size);
        }

        public void Push(int entityId)
        {
            if (!this._entities.Contains(entityId))
            {
                this._entities.Add(entityId);
            }
        }

        public void Remove(int entityId)
        {
            if (this._entities.Contains(entityId))
            {
                this._entities.Remove(entityId);
            }
        }

        public void Run()
        {
            var total = (double) this._entities.Count;
            var perPage = this._entitiesPerTask;
            var totalPages = Math.Ceiling(total / perPage);

            if (totalPages == 0)
            {
                return;
            }

            var parent = Task.Factory.StartNew(this.Process, 0);

            if (totalPages > 1)
            {
                Task task = null;
                
                for (var page = 1; page < totalPages; page++)
                {
                    if (task == null)
                    {
                        task = parent;
                    }
                    
                    task = task.ContinueWith((prev, p) =>
                    {
                        if (prev.IsCanceled)
                        {
                            throw new Exception("Whats wrong?");
                        }
                            
                        Process(p);
                    }, page);
                }   
            }
        }

        private void Process(object pageNum)
        {
            var page = (int) pageNum;
            var skip = page * _entitiesPerTask;
            var entities = this._entities
                .Skip(skip)
                .Take(_entitiesPerTask)
                .Select(x => this._entityPool.Get(x));
            
            var data = new TaskRunParameters(entities, this._system, page);
            
            foreach (var entity in data.Entities)
            {
                data.System.Execute(entity);
            }
            
            Console.WriteLine($"Thread {Task.CurrentId.ToString()}: page {data.Page.ToString()} finish.");
        }
    }
}