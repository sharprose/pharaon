namespace Anklave.ECS.Limousine.Systems
{
    using System;
    using System.Threading.Tasks;

    using ECS.Components;
    using ECS.Entities;
    using ECS.Systems;

    public abstract class AbstractEcsSystem : IEcsSystem
    {
        public abstract void Execute(IEcsEntity entity);
    }

    public abstract class AbstractEcsSystem<T> : AbstractEcsSystem, IEcsSystem<T>
        where T : class, IEcsComponent
    {
        public abstract void Execute(IEcsEntity entity, T component);

        public override void Execute(IEcsEntity entity)
        {
            var component = entity.GetComponent<T>();
            
            try
            {
                this.Execute(entity, component);
            }
            catch (Exception e)
            {
                #if DEBUG
                
                var system_type = this.GetType();
                var component_type = this.GetType();
                
                Console.WriteLine();
                Console.WriteLine($"Exception in '{system_type.Name}' [{system_type.FullName}]:");
                Console.WriteLine($"{e}");
                #endif
                
                throw;
            }
        }
    }
}