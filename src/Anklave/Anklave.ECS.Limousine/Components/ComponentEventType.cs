namespace Anklave.ECS.Limousine.Components
{
    public enum ComponentEventType
    {
        Add,
        Remove
    }
}