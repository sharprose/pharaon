namespace Anklave.ECS.Limousine.Components
{
    using ECS.Components;

    using Entities;

    public delegate void OnAddComponentEventHandler(ComponentEventType eventType, EcsEntity entity, IEcsComponent component);
}