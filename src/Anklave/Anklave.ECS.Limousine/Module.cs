namespace Anklave.ECS.Limousine
{
    using ECS.Services;

    using Gear.DI.Services;
    using Gear.Moduling;

    using Services;

    public class Module : IModule
    {
        public string Code => "Anklave.ECS.Limousine";

        public void Register(IContainer container)
        {
            container.Register<IEcsManager, EcsManager>();
        }
    }
}