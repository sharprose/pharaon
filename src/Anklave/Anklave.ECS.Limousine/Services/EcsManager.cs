namespace Anklave.ECS.Limousine.Services
{
    using Systems;

    using Common;

    using Components;

    using ECS.Common;
    using ECS.Components;
    using ECS.Entities;
    using ECS.Services;
    using ECS.Systems;

    using Entities;

    public class EcsManager : IEcsManager
    {
        private readonly IPool<EcsEntity> _entityPool;

        private readonly SystemMap _map;

        private readonly EcsSettings _settings;
        
        public EcsManager(EcsSettings settings)
        {
            this._settings = settings;
            this._entityPool = new EntitiesPool(settings.EntitiesPoolSize);
            this._map = new SystemMap(this._entityPool, settings);
        }
        
        public IEcsEntity AddEntity()
        {
            var entity = new EcsEntity(this._settings.ComponentCount);
            entity.OnRemove += this.OnRemoveEntity;
            entity.OnComponent += this.OnComponent;
            
            this._entityPool.Add(entity);
            
            return entity;
        }

        private void OnComponent(ComponentEventType eventType, EcsEntity entity, IEcsComponent component)
        {
            if (eventType == ComponentEventType.Add)
            {
                this._map.Push(entity, component);
            }
            else
            {
                this._map.Popup(entity.Id, component);   
            }
        }
        
        private void OnRemoveEntity(EcsEntity entity)
        {
            this._entityPool.Remove(entity.Id);
            
            entity.OnRemove -= this.OnRemoveEntity;
            entity.OnComponent -= this.OnComponent;
        }
        
        public void AddSystem<TS, TC>() 
            where TS : class, IEcsSystem<TC>, new() 
            where TC : class, IEcsComponent, new()
        {
            this._map.Register<TS, TC>();
        }

        public void Execute()
        {
            this._map.Execute();
        }
    }
}