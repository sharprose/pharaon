namespace Anklave.ECS.Limousine.Services
{
    public class EcsSettings
    {
        public int EntitiesPerThread = Constants.DEFAULT_ENTITIES_COUNT_PER_THREAD;

        public int EntitiesPoolSize = Constants.DEFAULT_COLLECTION_SIZE;

        public int ComponentCount = Constants.DEFAULT_COMPONENTS_COUNT;
    }
}