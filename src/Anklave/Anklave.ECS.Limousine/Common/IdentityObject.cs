namespace Anklave.ECS.Limousine.Common
{
    using ECS.Common;

    public abstract class IdentityObject : IIdentity, ISetIdentity
    {
        private int _id;

        public int Id => this._id;

        public void SetId(int id)
        {
            if (this._id == Constants.DEFAULT_IDENTIFY)
            {
                this._id = id;
            }
        }
    }
}