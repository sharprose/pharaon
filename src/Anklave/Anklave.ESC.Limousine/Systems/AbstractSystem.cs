namespace Anklave.ESC.Limousine.Systems
{
    using ECS.Components;
    using ECS.Entities;
    using ECS.Systems;

    public abstract class AbstractSystem<T> : IEscSystem<T>
        where T : class, IEscComponent
    {
        public abstract void Execute(IEcsEntity entity, T component);
    }
}