namespace Anklave.ESC.Limousine.Entities
{
    using ECS.Common;
    using ECS.Entities;

    public class BaseEntity : IEcsEntity, ISetIdentity
    {
        private long _id;

        private bool _isDisposed;
        
        public long Id => this._id;

        public void Dispose()
        {
            if (!this._isDisposed)
            {
                
                this._isDisposed = true;
            }
        }

        public void AddComponent<T>()
        {
            throw new System.NotImplementedException();
        }

        public void AddComponent<T>(T component)
        {
            throw new System.NotImplementedException();
        }

        public void RemoveComponent<T>()
        {
            throw new System.NotImplementedException();
        }

        public void SetId(long id)
        {
            this._id = id;
        }
    }
}