namespace Anklave.ECS.Common
{
    using System.Collections.Generic;

    public interface IPool<T>
        where T : IIdentity, ISetIdentity
    {
        IEnumerable<T> Items { get; }

        int Size { get; }
        
        T Get(int id);
        
        void Add(T item);

        void Remove(int id);

        void Clearout();
    }
}