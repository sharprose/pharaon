namespace Anklave.ECS.Common
{
    public interface ISetIdentity
    {
        void SetId(int id);
    }
}