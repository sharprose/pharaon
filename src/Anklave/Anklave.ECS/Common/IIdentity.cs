namespace Anklave.ECS.Common
{
    public interface IIdentity
    {
        int Id { get; }
    }
}