namespace Anklave.ECS.Systems
{
    using Components;

    using Entities;

    public interface IEcsSystem
    {
        void Execute(IEcsEntity entity);
    }
    
    public interface IEcsSystem<T> : IEcsSystem 
        where T : class, IEcsComponent
    {
        
    }
}