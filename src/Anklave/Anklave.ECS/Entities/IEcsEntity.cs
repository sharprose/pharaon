namespace Anklave.ECS.Entities
{
    using System;

    using Common;

    using Components;

    public interface IEcsEntity : IIdentity, IDisposable
    {
        IEcsEntity AddComponent<T>()
            where T : class, IEcsComponent, new();
        
        IEcsEntity AddComponent<T>(T component)
            where T : class, IEcsComponent;

        void RemoveComponent<T>()
            where T : class, IEcsComponent;

        T GetComponent<T>()
            where T : class, IEcsComponent;
    }
}