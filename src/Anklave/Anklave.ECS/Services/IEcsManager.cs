namespace Anklave.ECS.Services
{
    using Systems;

    using Components;

    using Entities;

    public interface IEcsManager
    {
        IEcsEntity AddEntity();

        void AddSystem<TS, TC>()
            where TS : class, IEcsSystem<TC>, new()
            where TC : class, IEcsComponent, new();

        void Execute();
    }
}