namespace Anklave.ECS.Jin.EcsSystem
{
    using Systems;

    using Components;

    using EcsComponent;

    using EcsEntity;

    using Entities;

    public abstract class AbstractEcsSystem : IEcsSystem
    {
        public abstract void Execute(IEcsEntity entity);
    }

    public abstract class AbstractEcsSystem<T> : AbstractEcsSystem, IEcsSystem<T> where T : class, IEcsComponent
    {
        protected readonly EntityPool _entityPool;

        protected readonly ComponentPool _componentPool;
        
        public override void Execute(IEcsEntity entity)
        {
            var component = entity.GetComponent<T>();

            this.Execute(entity, component);
        }

        public abstract void Execute(IEcsEntity entity, T component);
    }
}