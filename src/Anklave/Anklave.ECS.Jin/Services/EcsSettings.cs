namespace Anklave.ECS.Jin.Services
{
    using System;
    using System.Collections.Generic;

    using Systems;

    using EcsComponent;

    using EcsEntity;

    using Executor;

    public class EcsSettings
    {
        public int DefaultEntityPoolSize = Constants.DEFAULT_ENTITIES_POOL_SIZE;

        public int DefaultComponentPoolSize = Constants.DEFAULT_COMPONENT_POOL_SIZE;

        public Func<EntityPool, ComponentPool, Dictionary<Type, IEcsSystem>, IExecutor> Build;
    }
}