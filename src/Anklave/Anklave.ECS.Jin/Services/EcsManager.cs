namespace Anklave.ECS.Jin.Services
{
    using System;
    using System.Collections.Generic;

    using Systems;

    using Components;

    using ECS.Services;

    using EcsComponent;

    using EcsEntity;

    using Entities;

    using Executor;

    public class EcsManager : IEcsManager
    {
        private readonly ComponentPool _componentPool;

        private readonly Dictionary<Type, IEcsSystem> _componentSystem;

        private readonly EntityPool _entityPool;

        private IExecutor _execurot;
        
        public EcsManager(EcsSettings settings)
        {
            this._entityPool = new EntityPool(settings.DefaultEntityPoolSize);
            this._componentPool = new ComponentPool(settings.DefaultComponentPoolSize, settings.DefaultEntityPoolSize);

            this._componentSystem = new Dictionary<Type, IEcsSystem>();
            this._execurot = settings.Build(this._entityPool, this._componentPool, this._componentSystem);
        }

        public IEcsEntity AddEntity()
        {
            var result = new EcsEntity(this._componentPool, this._entityPool);

            return result;
        }

        public void AddSystem<TS, TC>()
            where TS : class, IEcsSystem<TC>, new()
            where TC : class, IEcsComponent, new()
        {
            var cmp = new TC();
            var typeCmp = cmp.GetType();

            var sys = new TS();

            if (!this._componentSystem.ContainsKey(typeCmp))
            {
                this._componentSystem.Add(typeCmp, sys);
            }
        }

        public void Execute()
        {
            this._execurot.Execute();

            this._entityPool.ClearOut();
            this._componentPool.ClearOut();
        }
    }
}