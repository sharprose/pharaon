namespace Anklave.ECS.Jin.EcsComponent
{
    using System.Collections.Generic;

    using Components;

    public sealed class ComponentPool
    {
        private readonly int _componentPoolSize;

        private Dictionary<int, ComponentCollection> _items;

        private List<int> _removed;
        
        private object _lockObject = new object();
        
        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="componentPoolSize">Size component pool.</param>
        /// <param name="entityCount">Size entity pool.</param>
        public ComponentPool(
            int componentPoolSize = Constants.DEFAULT_COMPONENT_POOL_SIZE,
            int entityCount = Constants.DEFAULT_COMPONENT_POOL_SIZE)
        {
            this._items = new Dictionary<int, ComponentCollection>(entityCount);
            this._componentPoolSize = componentPoolSize;
            this._removed = new List<int>(this._componentPoolSize);
        }

        public IEnumerable<IEcsComponent> Get(int entityId)
        {
            return this._items[entityId].Items;
        }
        
        public T Get<T>(int entityId)
            where T : class
        {
            if (this.Has<T>(entityId))
            {
                return this._items[entityId].Get<T>();
            }

            return null;
        }

        public bool Has<T>(int entityId)
            where T : class
        {
            if (this._items.ContainsKey(entityId))
            {
                return this._items[entityId].Has<T>();
            }
            
            return false;
        }

        public void Add(int entityId)
        {
            if (!this._items.ContainsKey(entityId))
            {
                this._items.Add(entityId, new ComponentCollection(this._componentPoolSize));   
            }
        }
        
        public void Add<T>(int entityId, T component)
            where T : class
        {
            this.Add(entityId);
            
            if (!Has<T>(entityId))
            {
                this._items[entityId].Add(component);
            }
        }

        public void Remove<T>(int entityId, T component)
            where T : class
        {
            this.Remove<T>(entityId);
        }

        public void Remove(int entityId)
        {
            if (this._items.ContainsKey(entityId))
            {
                this._items[entityId].Remove();
            }
        }
        
        public void Remove<T>(int entityId)
            where T : class
        {
            if (this._items.ContainsKey(entityId))
            {
                this._items[entityId].Remove<T>();
            }
        }

        public void Delete<T>()
            where T : class
        {
            
            this.AfterDelete();
        }

        public IEnumerable<IEcsComponent> List(int entityId)
        {
            if (this._items.ContainsKey(entityId))
            {
                return this._items[entityId].Items;
            }
            
            return null;
        }

        public void ClearOut()
        {
            foreach (var item in this._items)
            {
                item.Value.ClearOut();
            }
            
            this.AfterDelete();
        }

        //TODO: можно лучше, не за 2 прохода
        private void AfterDelete()
        {
            var forDelete = new List<int>();
            
            foreach (var entityComponentCollection in this._items)
            {
                if (entityComponentCollection.Value.IsEmpty)
                {
                    forDelete.Add(entityComponentCollection.Key);
                }
            }

            foreach (var id in forDelete)
            {
                this._items.Remove(id);
            }
        }
    }
}