namespace Anklave.ECS.Jin.EcsComponent
{
    using System;
    using System.Collections.Generic;

    using Components;

    internal class ComponentCollection
    {
        public ComponentCollection(int poolSize)
        {
            this._items = new Dictionary<Type, IEcsComponent>(poolSize);
            this._removed = new List<Type>(poolSize);
        }
        
        private Dictionary<Type, IEcsComponent> _items;

        private List<Type> _removed;
        
        private object _lockObject = new object();
        
        public bool Has<T>() 
            where T : class
        {
            var type = typeof(T);

            return this.Has(type);
        }

        public bool IsEmpty => this._items.Count == 0;

        public IEnumerable<IEcsComponent> Items => this._items.Values;

        public bool Has(Type type)
        {
            return this._items.ContainsKey(type);
        }

        public void ClearOut()
        {
            foreach (var key in this._removed)
            {
                this._items.Remove(key);
            }
            
            this._removed.Clear();
        }

        public void Remove()
        {
            foreach (var kvp in this._items)
            {
                if (!this._removed.Contains(kvp.Key))
                {
                    lock (_lockObject)
                    {
                        this._removed.Add(kvp.Key);
                    }
                }
            }
        }
        
        public void Remove<T>() 
            where T : class
        {
            var type = typeof(T);
            
            if(this._items.ContainsKey(type))
            {
                if (!this._removed.Contains(type))
                {
                    lock (_lockObject)
                    {
                        this._removed.Add(type);   
                    }   
                }
            }
        }

        public T Get<T>() 
                where T : class
        {
            var type = typeof(T);

            return this.Get<T>(type);
        }

        private T Get<T>(Type type)
            where T : class
        {
            if (this._items.ContainsKey(type))
            {
                return this._items[type] as T;
            }
            return null;
        }

        public void Add<T>(T component) 
            where T : class
        {
            if (!Has<T>())
            {
                this._items.Add(component.GetType(), component as IEcsComponent);
            }
        }
    }
}