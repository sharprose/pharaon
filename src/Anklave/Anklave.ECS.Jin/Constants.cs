namespace Anklave.ECS.Jin
{
    public static class Constants
    {
        public const int DEFAULT_ENTITIES_POOL_SIZE = 1024;
        
        public const int DEFAULT_COMPONENT_POOL_SIZE = 1024;

        public const int DEFAULT_EMPTY_ID = 0;

        public const bool EMPTY_ITEM = false;
        
        public const bool NONEMPTY_ITEM = true;
    }
}