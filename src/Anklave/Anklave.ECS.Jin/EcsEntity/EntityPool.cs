namespace Anklave.ECS.Jin.EcsEntity
{
    using System;
    using System.Collections.Generic;

    using Components;

    using Entities;

    public sealed class EntityPool
    {
        private List<int> _removed;

        private readonly int _poolSize;
        
        private readonly Dictionary<int, EcsEntity> _items;

        private object _lockObject = new object();

        public IEnumerable<EcsEntity> Items => this._items.Values;
        
        public EntityPool(int poolSize)
        {
            this._poolSize = poolSize;
            
            this._items = new Dictionary<int, EcsEntity>();
            this._removed = new List<int>(poolSize);
        }
        
        public int Add(EcsEntity entity)
        {
            if (this._items.Count >= this._poolSize)
            {
                throw new Exception("Max size entity pool.");
            }

            var id = -1;
            
            lock (_lockObject)
            {
                var newId = this.GetNewId();
                entity.SetId(newId);

                this._items[newId] = entity;
                id = newId;
            }
            
            return id;
        }

        public void Remove(int entityId)
        {
            if (!this._removed.Contains(entityId))
            {
                return;
            }

            lock (_lockObject)
            {
                this._removed.Add(entityId);    
            }
        }

        public void Delete(int entityId)
        {
            if (this._items.ContainsKey(entityId))
            {
                var item = this._items[entityId]; 
                this._items.Remove(entityId);
                    
                item.Dispose();
            }
        }
        
        public EcsEntity Get(int id)
        {
            var realId = id - 1;

            return this._items[realId];
        }

        public void ClearOut()
        {
            lock (_lockObject)
            {
                foreach (var id in this._removed)
                {
                    this.Delete(id);
                }
                
                this._removed.Clear();
            }
        }
        
        private int GetNewId()
        {
            int result = 0;

            for (var i = 0; i < this._poolSize; i++)
            {
                if (!this._items.ContainsKey(i))
                {
                    break;
                }
                
                result++;
            }

            return result;
        }
    }
}