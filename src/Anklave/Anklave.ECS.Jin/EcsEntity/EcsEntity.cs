namespace Anklave.ECS.Jin.EcsEntity
{
    using System.Collections.Generic;

    using Common;

    using Components;

    using EcsComponent;

    using Entities;

    public class EcsEntity : IdentityObject, IEcsEntity
    {
        public EcsEntity(ComponentPool componentPool, EntityPool entityPool)
        {
            this._componentPool = componentPool;
            entityPool.Add(this);
            
            this._componentPool.Add(this.Id);
        }
        
        private ComponentPool _componentPool;

        public IEnumerable<IEcsComponent> Components => this._componentPool.Get(this.Id);
        
        public void Dispose()
        {
            foreach (var component in this._componentPool.List(this.Id))
            {
                this._componentPool.Remove(this.Id, component);
            }

            this._componentPool = null;
        }

        public IEcsEntity AddComponent<T>()
            where T : class, IEcsComponent, new()
        {
            var instance = new T();
            
            this.AddComponent(instance);

            return this;
        }

        public IEcsEntity AddComponent<T>(T component)
            where T : class, IEcsComponent
        {
            this._componentPool.Add(this.Id, component);

            return this;
        }

        public void RemoveComponent<T>()
            where T : class, IEcsComponent
        {
            this._componentPool.Remove<T>(this.Id);
        }

        public T GetComponent<T>()
            where T : class, IEcsComponent
        {
            return this._componentPool.Get<T>(this.Id);
        }
    }
}