namespace Anklave.ECS.Jin.Common
{
    using ECS.Common;

    public abstract class IdentityObject : IIdentity, ISetIdentity
    {
        public int Id => this._id;

        private int _id = Constants.DEFAULT_EMPTY_ID;
        
        public void SetId(int id)
        {
            this._id = id;
        }
    }
}