namespace Anklave.ECS.Jin.Executor
{
    public interface IExecutor
    {
        void Execute();
    }
}