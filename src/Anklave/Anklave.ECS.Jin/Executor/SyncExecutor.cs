namespace Anklave.ECS.Jin.Executor
{
    using System;
    using System.Collections.Generic;

    using Systems;

    using EcsComponent;

    using EcsEntity;

    public class SyncExecutor : IExecutor
    {
        public void Set(EntityPool ep, Dictionary<Type, IEcsSystem> cs)
        {
            this._entityPool = ep;
            this._dictionary = cs;
        }

        private EntityPool _entityPool;

        private Dictionary<Type, IEcsSystem> _dictionary;
        
        public void Execute()
        {
            foreach (var entity in this._entityPool.Items)
            {
                foreach (var cmp in entity.Components)
                {
                    var type = cmp.GetType();
                    var sys = this._dictionary[type];
                    
                    sys.Execute(entity);
                }
            }
        }
    }
}