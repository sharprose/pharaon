namespace Anklave.ECS.Jin.Executor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Systems;

    using EcsComponent;

    using EcsEntity;

    public class AsyncExecutor : IExecutor
    {
        public AsyncExecutor(int entitiesPerThread, int countThreads)
        {
            this._threadsCount = countThreads;
            this._entitiesPerThread = entitiesPerThread;
        }
        
        private int _threadsCount;

        private int _entitiesPerThread;

        public void Set(EntityPool ep, ComponentPool cp, Dictionary<Type, IEcsSystem> cs)
        {
            this._entityPool = ep;
            this._componentPool = cp;
            this._dictionary = cs;
        }

        private EntityPool _entityPool;

        private ComponentPool _componentPool;

        private Dictionary<Type, IEcsSystem> _dictionary;
        
        public void Execute()
        {
            var total = (double)this._entityPool.Items.Count();
            var pages = Math.Ceiling(total / this._entitiesPerThread);
            
            var dicTasks = new Dictionary<int, List<Task>>();
            var index = 0;
            
            for (int i = 0; i < pages; i++)
            {
                var task = BuildTask(i);
                
                var mod = (i + 1) % this._threadsCount;

                if (mod == 0)
                {
                    index++;
                    
                }

                if (!dicTasks.ContainsKey(index))
                {
                    dicTasks.Add(index, new List<Task>());
                }
                
                dicTasks[index].Add(task);
            }
            
            Run(dicTasks);
        }

        private void Run(Dictionary<int, List<Task>> dic)
        {
            foreach (var kvp in dic)
            {
                var tasks = kvp.Value;
                var main = Task.WhenAll(tasks);

                main.Wait();
            }
        }

        private Task BuildTask(int page)
        {
            var task = Task.Run(() =>
            {
                var skip = (page - 1) * this._entitiesPerThread;
                var entities = this._entityPool.Items
                    .Skip(skip)
                    .Take(this._entitiesPerThread);
                
                foreach (var entity in entities)
                {
                    foreach (var cmp in entity.Components)
                    {
                        var type = cmp.GetType();

                        var sys = this._dictionary[type];
                    
                        sys.Execute(entity);
                    }
                }
            });

            return task;
        }
    }
}