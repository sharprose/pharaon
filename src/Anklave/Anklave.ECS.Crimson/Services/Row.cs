namespace Anklave.ECS.Crimson.Services
{
    using Components;

    using Entities;

    public class Row
    {
        public IEcsEntity Entity { get; }
        
        public bool ToRemove { get; }
        
        private IEcsComponent[] _components { get; }
    }
}