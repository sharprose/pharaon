namespace Anklave.ECS.Tests.Systems.Jin
{
    using System;

    using Components;

    using ECS.Jin.EcsSystem;

    using Entities;

    public class TestSystemB: AbstractEcsSystem<CmpB>
    {
        public override void Execute(IEcsEntity entity, CmpB component)
        {
            if (component.A <= Int32.MaxValue)
            {
                component.A++;
            }
        }
    }
}