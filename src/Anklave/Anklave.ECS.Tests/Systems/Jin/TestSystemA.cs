namespace Anklave.ECS.Tests.Systems.Jin
{
    using System;

    using Components;

    using ECS.Jin.EcsSystem;

    using Entities;

    public class TestSystemA : AbstractEcsSystem<CmpA>
    {
        public override void Execute(IEcsEntity entity, CmpA component)
        {
            if (component.A <= Int32.MaxValue)
            {
                component.A++;
            }
        }
    }
}