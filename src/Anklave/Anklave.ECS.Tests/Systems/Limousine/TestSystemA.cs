namespace Anklave.ECS.Tests.Systems.Limousine
{
    using System;

    using Components;

    using ECS.Limousine.Systems;

    using Entities;

    public class TestSystemA : AbstractEcsSystem<CmpA>
    {
        public override void Execute(IEcsEntity entity, CmpA component)
        {
            if (component.A <= Int32.MaxValue)
            {
                component.A++;
            }
        }
    }
}