namespace Anklave.ECS.Tests.Systems.Limousine
{
    using System;

    using Components;

    using Entities;

    using ECS.Limousine.Systems;

    public class TestSystemB : AbstractEcsSystem<CmpB>
    {
        public override void Execute(IEcsEntity entity, CmpB component)
        {
            if (component.A <= Int32.MaxValue)
            {
                component.A++;
            }
        }
    }
}