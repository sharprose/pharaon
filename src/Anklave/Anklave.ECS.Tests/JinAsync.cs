namespace Anklave.ECS.Tests
{
    using Systems.Jin;

    using Components;

    using Jin.Executor;
    using Jin.Services;

    using NUnit.Framework;

    using Services;

    [TestFixture]
    public class JinAsync: BaseEcsTests
    {
        protected override IEcsManager BuildManager()
        {
            var manager = new EcsManager(new EcsSettings()
            {
                DefaultComponentPoolSize = 100,
                DefaultEntityPoolSize = 10000,
                Build = (pool, componentPool, arg3) =>
                {
                    var result = new AsyncExecutor(100, 8);
                    result.Set(pool, componentPool, arg3);
                    
                    return result;
                } 
            });

            manager.AddSystem<TestSystemA, CmpA>();
            manager.AddSystem<TestSystemB, CmpB>();
            
            return manager;
        }

        [Test]
        [TestCase(1000)]
        public override void SingleRun(int entitiesCount)
        {
            var manager = this.PrepareManager(entitiesCount);
            
            manager.Execute();
        }

        [Test]
        [TestCase(1000, 30)]
        [TestCase(1000, 60)]
        public override void MultipleRun(int entitiesCount, int runCount)
        {
            var manager = this.PrepareManager(entitiesCount);

            for (int i = 0; i < runCount; i++)
            {
                manager.Execute();   
            }
        }
    }
}