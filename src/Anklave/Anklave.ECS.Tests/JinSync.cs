namespace Anklave.ECS.Tests
{
    using Systems.Jin;

    using Components;

    using ECS.Jin.Executor;
    using ECS.Jin.Services;

    using NUnit.Framework;

    using Services;

    [TestFixture]
    public class JinSync : BaseEcsTests
    {
        protected override IEcsManager BuildManager()
        {
            var manager = new EcsManager(new EcsSettings()
            {
                DefaultComponentPoolSize = 100,
                DefaultEntityPoolSize = 10000,
                Build = (pool, componentPool, arg3) =>
                {
                    var result = new SyncExecutor();
                    result.Set(pool, arg3);
                    
                    return result;
                } 
            });

            manager.AddSystem<TestSystemA, CmpA>();
            manager.AddSystem<TestSystemB, CmpB>();
            
            return manager;
        }

        [Test]
        [TestCase(1000)]
        public override void SingleRun(int entitiesCount)
        {
            var manager = this.PrepareManager(entitiesCount);
            
            manager.Execute();
        }

        [Test]
        [TestCase(1000, 30)]
        [TestCase(1000, 60)]
        public override void MultipleRun(int entitiesCount, int runCount)
        {
            var manager = this.PrepareManager(entitiesCount);

            for (int i = 0; i < runCount; i++)
            {
                manager.Execute();   
            }
        }
    }
}