namespace Anklave.ECS.Tests
{
    using System;

    using Systems;
    using Systems.Limousine;

    using Components;

    using NUnit.Framework;

    using Services;

    public abstract class BaseEcsTests
    {
        protected abstract IEcsManager BuildManager();

        protected IEcsManager PrepareManager(int entitiesCount)
        {
            var manager = this.BuildManager();
            
            for (int i = 0; i < entitiesCount; i++)
            {
                manager
                    .AddEntity()
                    .AddComponent<CmpA>()
                    .AddComponent<CmpB>();
            }

            return manager;
        }

        public abstract void SingleRun(int entitiesCount);
        
        public abstract void MultipleRun(int entitiesCount, int runCount);
    }
}