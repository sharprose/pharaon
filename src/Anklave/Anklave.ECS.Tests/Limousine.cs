namespace Anklave.ECS.Tests
{
    using System.Security.AccessControl;

    using Systems;
    using Systems.Limousine;

    using Components;

    using ECS.Limousine.Services;

    using NUnit.Framework;

    using Services;

    [TestFixture]
    public class Limousine : BaseEcsTests
    {
        protected override IEcsManager BuildManager()
        {
            var manager = new Anklave.ECS.Limousine.Services.EcsManager(new EcsSettings()
            {
                ComponentCount = 50,
                EntitiesPerThread = 100,
                EntitiesPoolSize = 10000
            });

            manager.AddSystem<TestSystemA, CmpA>();
            manager.AddSystem<TestSystemB, CmpB>();
            
            return manager;
        }

        [Test]
        [TestCase(1000)]
        public override void SingleRun(int entitiesCount)
        {
            var manager = this.PrepareManager(entitiesCount);
            
            manager.Execute();
        }

        [Test]
        [TestCase(1000, 30)]
        [TestCase(1000, 60)]
        public override void MultipleRun(int entitiesCount, int runCount)
        {
            var manager = this.PrepareManager(entitiesCount);

            for (int i = 0; i < runCount; i++)
            {
                manager.Execute();   
            }
        }
    }
}