namespace Anklave.ECS.Tests.Components
{
    using ECS.Components;

    public class CmpA : IEcsComponent
    {
        public int A { get; set; }
    }
}