namespace Anklave.ECS.Tests.Components
{
    using ECS.Components;

    public class CmpB: IEcsComponent
    {
        public int A { get; set; }
    }
}